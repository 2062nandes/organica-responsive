<?php require_once 'header.php'; ?>
<body id="inicio">
<?php require('menu.php') ?>
<div class="wrapeverything orswrapper">
	<div class="wrap maincontent">
			<h2 id="corporativos" class="tituloProd">Corporativos</h2>
			<div class="col100">
      <h3 id="personzalizada">LINEA PERSONALIZADA <a href="#linea-personalizada"> <i class="ojo fa fa-eye" aria-hidden="true"></i></a></h3>
      <ul class="lb-album">
        <li class="col20d">
          <a href="#personalizada-1">
            <img src="img/productos/papas-bolivar.png" width="135" height="135" alt="personalizada01">
            <span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
          </a>
					<p>Papas Bolívar</p>
					<div class="lb-overlay " id="personalizada-1">
					<div class="line-inner">
						<div class="freewrapper1">
			            <div class="coll1">
			              <h2>Papas Bolívar</h2>
			              <img class="lineimg" src="img/productos/papas-bolivar.png" alt="personalizada01">
										<a href="#personalizada-2" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#personalizada-2" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
			            </div>
			            <div class="coll2 linea-scroll">
			              <h3>LINEA PERSONALIZADA</h3>
			              <div class="line-links">
			                  <img class="lineimg" src="img/productos/papas-bolivar.png" alt="producto">
			                </div>
			                  <div class="col-descripcion"><strong>Presentacion</strong>
			                  <p> </p>
			                  <strong>Descripcion</strong>
			                  <p>

			                  </p>
			            </div>
          			</div>
								<a href="#personalizada" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
        </li>
        <li class="col20d">
					<a href="#personalizada-2">
            <img src="img/productos/papas-wilsterman.png" width="135" height="135" alt="personalizada02">
            <span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
          </a>
					<p>Papas Wilsterman</p>
					<div class="lb-overlay " id="personalizada-2">
					<div class="line-inner">
						<div class="freewrapper1">
			            <div class="coll1">
			              <h2>Papas Wilsterman</h2>
			              <img class="lineimg" src="img/productos/papas-wilsterman.png" alt="personalizada02">
										<a href="#personalizada-1" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#personalizada-1" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
			            </div>
			            <div class="coll2 linea-scroll">
			              <h3>LINEA PERSONALIZADA</h3>
			              <div class="line-links">
			                  <img class="lineimg" src="img/productos/papas-wilsterman.png" alt="producto">
			                </div>
			                  <div class="col-descripcion"><strong>Presentacion</strong>
			                  <p> </p>
			                  <strong>Descripcion</strong>
			                  <p>

			                  </p>
			            </div>
          			</div>
								<a href="#personalizada" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
        </li>
			</ul>
				<?php require('linea.php'); ?>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<?php require_once 'footer.php'; ?>
