<?php require_once 'header.php'; ?>
<body id="inicio">
<?php require('menu.php') ?>
<div class="wrapeverything orswrapper">
	<div class="wrap maincontent">
		<h2 id="cereales" class="tituloProd">Cereales</h2>
		<div class="col100">
			<h3 id="c-real">LINEA C-REAL <a href="#linea-c-real"> <i class="ojo fa fa-eye" aria-hidden="true"></i></a></h3>
      <ul class="lb-album">
        <li class="col20d">
          <a href="#c-real-1">
            <img src="img/productos/aritos-de-frutas.png" width="135" height="135" alt="c-real01">
            <span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
          </a>
					<p>Aritos de frutas</p>
					<div class="lb-overlay " id="c-real-1">
					<div class="line-inner">
						<div class="freewrapper1">
			            <div class="coll1">
			              <h2>Aritos de frutas</h2>
			              <img class="lineimg" src="img/productos/aritos-de-frutas.png" alt="c-real01">
										<a href="#c-real-2" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#c-real-2" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
			            </div>
			            <div class="coll2 linea-scroll">
			              <h3>LINEA C-REAL</h3>
			              <div class="line-links">
			                  <img class="lineimg" src="img/productos/aritos-de-frutas.png" alt="producto">
			              </div>
			                  <strong>Presentacion</strong>
			                  <p> 30g, 300g</p>
			                  <strong>Descripcion</strong>
			                  <p>
			                      Producto obtenido a base de una mezcla harinas de cereales (trigo, maíz, avena y arroz) que pasan por un proceso de extrusión en forma de aros, horneado y saborizado.
			                  </p>
			            </div>
          			</div>
								<a href="#c-real" class="lb-close modal-close"></i></a>
							</div>
						</div>
        </li>
        <li class="col20d">
					<a href="#c-real-2">
            <img src="img/productos/bolitas-de-chocolate.png" width="135" height="135" alt="c-real02">
            <span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
          </a>
					<p>Bolitas de chocolates</p>
					<div class="lb-overlay " id="c-real-2">
					<div class="line-inner">
						<div class="freewrapper1">
			            <div class="coll1">
			              <h2>Bolitas de chocolates</h2>
			              <img class="lineimg" src="img/productos/bolitas-de-chocolate.png" alt="c-real02">
										<a href="#c-real-1" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#c-real-1" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
			            </div>
			            <div class="coll2 linea-scroll">
			              <h3>LINEA C-REAL</h3>
			              <div class="line-links">
			                  <img class="lineimg" src="img/productos/bolitas-de-chocolate.png" alt="producto">
			                </div>
			                  <strong>Presentacion</strong>
			                  <p> 30g, 300g</p>
			                  <strong>Descripcion</strong>
			                  <p>
			                    Producto obtenido a base de una mezcla harinas de cereales (trigo, maíz, avena y arroz) que pasan por un proceso de extrusión en forma de bolitas, horneado y saborizado a chocolate.
			                  </p>
			            </div>
          			</div>
								<a href="#c-real" class="lb-close modal-close"></i></a>
							</div>
						</div>
        </li>
			</ul>
		</div>
		<!------------------------------------------------------------------------------------------>
		<div class="col100">
			<h3 id="sr-maiz">LINEA SR-MAIZ <a href="#linea-sr-maiz"> <i class="ojo fa fa-eye" aria-hidden="true"></i></a></h3>
		<ul class="lb-album">
			<li class="col20d">
				<a href="#sr-maiz-1">
					<img src="img/productos/maiz-sr-maiz-cebolla-taper.png" width="135" height="135" alt="sr-maiz01">
					<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
				</a>
				<p>Maíz Sr. Maíz cebolla taper</p>
				<div class="lb-overlay " id="sr-maiz-1">
				<div class="line-inner">
					<div class="freewrapper1">
								<div class="coll1">
									<h2>Maíz Sr. Maíz cebolla taper</h2>
									<img class="lineimg" src="img/productos/maiz-sr-maiz-cebolla-taper.png" alt="sr-maiz01">
									<a href="#sr-maiz-2" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
									<a href="#sr-maiz-2" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
								</div>
								<div class="coll2 linea-scroll">
									<h3>LINEA SR-MAIZ</h3>
									<div class="line-links">
											<img class="lineimg" src="img/productos/maiz-sr-maiz-cebolla-taper.png" alt="producto">
										</div>
											<strong>Presentacion</strong>
											<p> 100g</p>
											<strong>Descripcion</strong>
											<p>
													Producto a base del maíz blanco pelado, seleccionado, humedecido, fritado con una adición de sal y saborizante.
											</p>
								</div>
							</div>
							<a href="#sr-maiz" class="lb-close modal-close"></i></a>
						</div>
				</div>
			</li>
			<li class="col20d">
				<a href="#sr-maiz-2">
					<img src="img/productos/mani-con-pasas.png" width="135" height="135" alt="sr-maiz02">
					<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
				</a>
				<p>Maní con pasas zipper</p>
				<div class="lb-overlay " id="sr-maiz-2">
				<div class="line-inner">
					<div class="freewrapper1">
								<div class="coll1">
									<h2>Maní con pasas zipper</h2>
									<img class="lineimg" src="img/productos/mani-con-pasas.png" alt="sr-maiz02">
									<a href="#sr-maiz-1" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
									<a href="#sr-maiz-1" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
								</div>
								<div class="coll2 linea-scroll">
									<h3>LINEA SR-MAIZ</h3>
									<div class="line-links">
											<img class="lineimg" src="img/productos/mani-con-pasas.png" alt="producto">
										</div>
											<strong>Presentacion</strong>
											<p> 100g</p>
											<strong>Descripcion</strong>
											<p>
												Producto a base de maní (Arachis hypogaea) seleccionado y pelado completamente, el cual pasa por un proceso de fritado y posterior adición de sal y uvas (Vitis vinífera) pasas.
											</p>
								</div>
							</div>
							<a href="#sr-maiz" class="lb-close modal-close"></i></a>
						</div>
				</div>
			</li>
		</ul>
	</div>
	<?php require('linea.php'); ?>
	</div>
	</div>
	<div class="clear"></div>
	<?php require_once 'footer.php'; ?>
