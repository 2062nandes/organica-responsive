<!DOCTYPE HTML>
<html lang="es">
<head>
    <title>ORGÁNICA DEL SUR S.R.L.</title>
  <link rel="icon" href="/favicon.png" type="image/png">
    <meta charset="utf-8">
    <meta name="designer" content="Fernando Averanga Aruquipa">
    <meta name="description" content="Fabricación de productos alimenticios: Snacks: papas fritas, papas fritas sabrositas picantes, papas hiladas mediana, manís salados horneados. Cereales: Aritos de frutas, bolitas de chocolates, Maíz Sr.  Maíz cebolla taper, maní con pasas zipper, papas Bolívar, papas Wilsterman. Gramineas: Arroz, avenas de sopa, azúcar impalpable, chuños negros, cocos rallados, garbanzos, harina blanca, jankaquipas, lawas de choclos, lawas de chuños, lentejas, linaza molidas, maíz para somo, maíz pipocas, manís pelados, mocochinchis, pan duros, porotos blancos, porotos negros, sémola blancas, tojoris, trigos enteros, trigos partidos, tuntas.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/resetv2.0.css"/>
    <link rel="stylesheet" type="text/css" href="css/animate.css"/>
    <link rel="stylesheet" type="text/css" href="css/hint.min.css"/>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <link rel="stylesheet" type="text/css" href="css/responsivo.css"/>
    <link rel="stylesheet" href="css/lightbox.css">
    <link rel="stylesheet" type="text/css" href="fontaw/font-awesome4.5.0.min.css">
    <link rel="stylesheet" type="text/css" href="sliderengine/amazingslider-1.css">
    <!-- Insert to your webpage before the </head> -->
    <script type="text/javascript" src="js/jquery1.12.1.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <script src="sliderengine/amazingslider.js"></script>
    <script src="sliderengine/initslider-1.js"></script>
    <!-- End of head section HTML codes -->
    <script type="text/javascript" src="js/headroom.js"></script>
    <script type="text/javascript" src="js/js.js"></script>
    <!--[if lt IE 9]>
      <script type="text/javascript" src="js/html5shiv3.7.3.min.js"></script>
      <script type="text/javascript" src="js/respond1.4.2.min.js"></script>
    <![endif]-->
</head>
