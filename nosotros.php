<?php require_once 'header.php'; ?>
<body id="inicio">
<?php require('menu.php') ?>
<div class="wrapeverything orswrapper">
	<div class="wrap maincontent">
      <h2 id="nosotros" class="tituloProd">Nosotros</h2>
			<h3 class="first">Quiénes somos</h3>
	  	<div class="content">
				<p class="center">Somos una empresa dedicada a la fabricación de productos alimenticios, snacks, cereales y gramíneas. Contamos con varias líneas de productos comerciales, para satisfacer el paladar de nuestros consumidores:<br>
				<b>Snacks</b>: papas fritas, papas fritas sabrositas picantes, papas hiladas mediana, manís salados horneados.<br>
				<b>Cereales</b>: Aritos de frutas, bolitas de chocolates, Maíz Sr.  Maíz cebolla taper, maní con pasas zipper, papas Bolívar, papas Wilsterman.<br>
				<b>Gramíneas</b>: Arroz, avenas de sopa, azúcar impalpable, chuños negros, cocos rallados, garbanzos, harina blanca, jankaquipas, lawas de choclos, lawas de chuños, lentejas, linaza molidas, maíz para somo, maíz pipocas, manís pelados, mocochinchis, pan duros, porotos blancos, porotos negros, sémola blancas, tojoris, trigos enteros, trigos partidos, tuntas.
				</p>
			</div>
      <div class="col60">
        <h3>Visión</h3>
        <div class="content">
          <p>Ser la empresa líder a nivel nacional y reconocida a nivel internacional en la producción y comercialización de productos alimenticios, contando con recursos humanos altamente comprometidos y enmarcándose en la innovación permanentemente.</p>
        </div>
        <h3>Misión</h3>
        <div class="content">
          <p>Ofertar productos alimenticios a clientes que demandan altos estándares de calidad e inocuidad, soportada en política y procesos. Generando oportunidades de crecimiento a nuestro personal, socios, clientes y comunidad enmarcados en la honestidad, compromiso, equidad, integridad y trabajo en equipo. </p>
        </div>
      </div>
      <div class="col40" style="float: right;">
          <h3>Video presentación</h3>
					<iframe style="width:100%;height:320px" src="https://www.youtube.com/embed/t2tC3BvzoYY" frameborder="0" allowfullscreen></iframe>
			</div>
      <div style="clear: both"></div>
      <h3>Política de inocuidad</h3>
      <div class="content">
        <p>En <b>ORGÁNICA DEL SUR</b> tenemos el compromiso de brindar productos alimenticios inocuos que satisfagan las necesidades y requerimientos de nuestros clientes, buscando la mejora continua en nuestros procesos en términos de calidad, inocuidad, costo y oportunidad. En conformidad con los requisitos legales y normas internacionales.</p>
      </div>
			<?php require('linea.php'); ?>
    </div>
</div>
<div class="clear"></div>
<?php require_once 'footer.php'; ?>
