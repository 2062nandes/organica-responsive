<header id="header" class="header fixed">
	<div class="wrapeverything">
		<div class="colh1">
			<div class="wraplogo">
				<span class="fiesta"></span>
				<span class="fiesta2"></span>
				<a class="jlink max-organica" href="#inicio">
							<div class="globo">
								<div class="globe-container">
							    	<div class="globe">
							    	</div>
								</div>
								<img class="ifiesta logoimg" src="img/01-rganica.png"/>
							</div>
				</a>
			</div>
		</div>
		<img class="mini-organica logoimg" src="img/organica.png"/>
		<div class="colhi1">
			<div class="topsocial">
				<ul>
					<li><a target="_blank" onclick="return true;" href="https://www.facebook.com/OrganicaSRL">
					</a></li>
					<li><a target="_blank" onclick="return false;" href="#">
					</a></li>
					<li><a target="_blank" onclick="return true;" href="https://www.youtube.com/watch?v=t2tC3BvzoYY">
					</a></li>
				</ul>
			</div>
		</div>
		<div class="colhi2">
			<div class="topnav">
				<ul>
					<li><a class="jlink" href="index.php">
						<i class="fa fa-3x fa-home" aria-hidden="true"></i>
						<span>Inicio</span></a></li>
					<li><a class="jlink" href="nosotros.php">
						<i class="fa fa-3x fa-users" aria-hidden="true"></i>
						<span>Nosotros</span></a></li>
					<li><a class="jlink" href="contacto.php">
						<i class="fa fa-3x fa-envelope" aria-hidden="true"></i>
						<span>Contactos</span></a></li>
				</ul>
			</div>
		</div>

		<div class="colh2">
			<div class="freewrapper" id="menu-cabecera">
			</div>
			<div class="freewrapper">
				<nav>
					<ul>
						<div class="dropdown">&nbsp;</div>

						<div class="dropdown"><a class="btn-menus" href="snacks.php"><span class="jlink" data-jlink="#snacks">SNACKS</span></a>
							<div class="inner boton1">
								<div class="arrow triangulo"><i class="fa fa-caret-up"></i></div>
								<div class="freewrapper">
									<div class="arrowrapper">
										<div class="colaw1">&nbsp;</div>
										<div class="colaw3">&nbsp;</div>
										<div class="colaw4">&nbsp;</div>
										<div class="colaw5">&nbsp;</div>
										<div class="colaw6">&nbsp;</div>
									</div>
									<div class="clear"></div>
									<div class="col20">
										<ul>
											<li><a href="#linea-sabrositas"><span>Línea Sabrositas</span></a></li>
											<li><a href="snacks.php#sabrosita-1"><span>Papas fritas sabrositas</span></a></li>
		                  <li><a href="snacks.php#sabrosita-2"><span>Papas fritas sabrositas picantes</span></a></li>
		                  <li><a href="snacks.php#sabrosita-3"><span>Papas hiladas mediana</span></a></li>
		                  <li><a href="snacks.php#sabrosita-4"><span>Manís salados horneados</span></a></li>
		                </ul>
									</div>
									<div class="col20">
										<ul>
											<li><a href="#linea-fiesta"><span>Línea Fiesta</span></a></li>
											<li><a href="snacks.php#fiesta-1"><span>Chesitos fiesta</span></a></li>
											<li><a href="snacks.php#fiesta-2"><span>Nachos fiesta clasico</span></a></li>
										<!--	<li><a href="snacks.php#fiesta-3"><span>Nachos fiesta picante</span></a></li> -->
											<li><a href="snacks.php#fiesta-4"><span>Nachos fiestas cheddar</span></a></li>
											<li><a href="snacks.php#fiesta-5"><span>Nachos jalapeños</span></a></li>
											<li><a href="snacks.php#fiesta-6"><span>Pipocas fiesta con miel</span></a></li>
										</ul>
									</div>
									<div class="col20">
										<ul>
											<li><a href="#linea-gourmet"><span>Línea Gourmet</span></a></li>
											<li><a href="snacks.php#gourmet-1"><span>Papas gourmet churrasco</span></a></li>
											<li><a href="snacks.php#gourmet-2"><span>Papas gourmet k'allu</span></a></li>
											<li><a href="snacks.php#gourmet-3"><span>Papas gourmet cordero al palo</span></a></li>
											<li><a href="snacks.php#gourmet-4"><span>Papas gourmet frutos de mar</span></a></li>
										</ul>
									</div>
									<div class="col20">
										<ul>
											<li><a href="#linea-recreo"><span>Línea Recreo</span></a></li>
											<!--<li><a href="snacks.php#recreo-1"><span>Nachos recreo tradicional</span></a></li>-->
											<li><a href="snacks.php#recreo-2"><span>Nachos recreo picantes</span></a></li>
											<li><a href="snacks.php#recreo-3"><span>Nachos recreo cheddar</span></a></li>
										<!--
											<li><a href="snacks.php#recreo-4"><span>Pipocas recreo con miel</span></a></li>
											<li><a href="snacks.php#recreo-5"><span>Papas recreo clásicas</span></a></li>
											<li><a href="snacks.php#recreo-6"><span>Papas recreo picantes</span></a></li>
											<li><a href="snacks.php#recreo-7"><span>Papas recreo limón</span></a></li>
											<li><a href="snacks.php#recreo-8"><span>Papas recreo cheddar</span></a></li>
										-->
										</ul>
									</div>
									<div class="col20">
										<ul>
											<li><a href="#linea-tossitos"><span>Línea Tossitos</span></a></li>
											<li><a href="snacks.php#tossitos-pasankalla"><span>Tossitos pasank'allas</span></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="dropdown"><a class="btn-menus" href="cereales.php"><span class="jlink" data-jlink="#cereales">CEREALES</span></a>
							<div class="inner boton2">
								<div class="arrow triangulo"><i class="fa fa-caret-up"></i></div>
								<div class="freewrapper">
									<div class="arrowrapper">
										<div class="colaw1">&nbsp;</div>
										<div class="colaw2">&nbsp;</div>
										<div class="colaw4">&nbsp;</div>
										<div class="colaw5">&nbsp;</div>
										<div class="colaw6">&nbsp;</div>
									</div>
									<div class="clear"></div>
									<div class="col50">
										<ul>
											<li><a href="#linea-c-real"><span>Línea C-Real</span></a></li>
											<li><a href="cereales.php#c-real-1"><span>Aritos de frutas</span></a></li>
											<li><a href="cereales.php#c-real-2"><span>Bolitas de chocolates</span></a></li>
		                                </ul>
									</div>
									<div class="col50">
										<ul>
											<li><a href="#linea-sr-maiz"><span>Línea Sr. Maíz</span></a></li>
											<li><a href="cereales.php#sr-maiz-1"><span>Maíz Sr. Maíz cebolla taper</span></a></li>
											<li><a href="cereales.php#sr-maiz-2"><span>Maní con pasas</span></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="dropdown"><a class="btn-menus" href="gramineas.php"><span class="jlink" data-jlink="#gramineas">GRAMÍNEAS</span></a>
							<div class="inner boton3">
								<div class="arrow"><i class="fa fa-caret-up"></i></div>
								<!--<div class="arrow triangulo"><i class="fa fa-caret-up"></i></div>-->
								<div class="freewrapper">
									<div class="arrowrapper">
										<div class="colaw1">&nbsp;</div>
										<div class="colaw2">&nbsp;</div>
										<div class="colaw3">&nbsp;</div>
										<div class="colaw5">&nbsp;</div>
										<div class="colaw6">&nbsp;</div>
									</div>
									<div class="clear"></div>
									<div class="col25">
										<ul>
											<li><a href="#linea-en-grano"><span>En grano</span></a></li>
											<li><a href="gramineas.php#en-grano-1"><span>Arroz</span></a></li>
											<li><a href="gramineas.php#en-grano-2"><span>Azúcar impalpable</span></a></li>
											<li><a href="gramineas.php#en-grano-3"><span>Garbanzos</span></a></li>
											<li><a href="gramineas.php#en-grano-4"><span>Lentejas</span></a></li>
											<li><a href="gramineas.php#en-grano-5"><span>Trigos enteros</span></a></li>
											<li><a href="gramineas.php#en-grano-6"><span>Trigos partidos</span></a></li>
											<li><a href="gramineas.php#en-grano-7"><span>Maíz para somo</span></a></li>
											<li><a href="gramineas.php#en-grano-8"><span>Maíz pipocas</span></a></li>
										</ul>
									</div>
									<div class="col25">
										<ul>
											<li><a href="#linea-molidos"><span>Molidos</span></a></li>
											<li><a href="gramineas.php#molidos-1"><span>Harina blanca</span></a></li>
											<li><a href="gramineas.php#molidos-2"><span>Lawa de choclo</span></a></li>
											<li><a href="gramineas.php#molidos-3"><span>Lawa de chuño</span></a></li>
											<li><a href="gramineas.php#molidos-4"><span>Linazas molidas</span></a></li>
											<li><a href="gramineas.php#molidos-5"><span>Pan duro</span></a></li>
											<li><a href="gramineas.php#molidos-6"><span>Sémolas blancas</span></a></li>
											<li><a href="gramineas.php#molidos-7"><span>Tojoris</span></a></li>
											<li><a href="gramineas.php#molidos-8"><span>Avenas de sopa</span></a></li>
											<li><a href="gramineas.php#molidos-9"><span>Jankaquipa</span></a></li>
											<li><a href="gramineas.php#molidos-10"><span>Cocos rallados</span></a></li>
										</ul>
									</div>
									<div class="col25">
										<ul>
											<li><a href="#linea-deshidratados"><span>Deshidratados</span></a></li>
											<li><a href="gramineas.php#deshidratado-1"><span>Chuños negros</span></a></li>
											<li><a href="gramineas.php#deshidratado-2"><span>Mocochinchi</span></a></li>
											<li><a href="gramineas.php#deshidratado-3"><span>Tuntas</span></a></li>
										</ul>
									</div>
									<div class="col25">
										<ul>
											<li><a href="#linea-semillas"><span>Semillas</span></a></li>
											<li><a href="gramineas.php#semilla-1"><span>Maní con cáscara</span></a></li>
											<li><a href="gramineas.php#semilla-2"><span>Maní pelado</span></a></li>
											<li><a href="gramineas.php#semilla-3"><span>Porotos blancos</span></a></li>
											<li><a href="gramineas.php#semilla-4"><span>Porotos negros</span></a></li>
											<li><a href="gramineas.php#semilla-5"><span>Porotos rojos</span></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="dropdown"><a class="btn-menus" href="corporativos.php"><span class="jlink" data-jlink="#corporativos">CORPORATIVOS</span></a>
							<div class="inner boton4">
								<div class="arrow"><i class="fa fa-caret-up"></i></div>
								<div class="freewrapper">
									<div class="arrowrapper">
										<div class="colaw1">&nbsp;</div>
										<div class="colaw2">&nbsp;</div>
										<div class="colaw3">&nbsp;</div>
										<div class="colaw4">&nbsp;</div>
										<div class="colaw6">&nbsp;</div>
									</div>
									<div class="clear"></div>
									<div class="col100">
										<ul>
											<li><a href="#linea-personalizada"><span>Línea Personalizada</span></a></li>
											<li><a href="corporativos.php#personalizada-1"><span>Papas Bolívar</span></a></li>
											<li><a href="corporativos.php#personalizada-2"><span>Papas Wilsterman</span></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="dropdown"><a class="btn-menus" href="#catalogo"><span class="jlink" data-jlink="#catalogo">CATÁLOGO</li></span></a>
							<div class="inner boton5">
								<div class="arrow"><i class="fa fa-caret-up"></i></div>
								<div class="freewrapper">
									<div class="arrowrapper">
										<div class="colaw1">&nbsp;</div>
										<div class="colaw2">&nbsp;</div>
										<div class="colaw3">&nbsp;</div>
										<div class="colaw4">&nbsp;</div>
										<div class="colaw5">&nbsp;</div>
									</div>
									<div class="clear"></div>
									<div class="col100">
										<ul>
											<li><a href="#catalogo"><span>CATALOGO</span></a></li>
											<li><a href="#listado1"><span>Listado 1</span></a></li>
											<li><a href="#listado2"><span>Listado 2</span></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</ul>
				</nav>
			</div>
		</div>
		<div class="colMenu">
			<div class="contenedor-menu">
				<a class="menures" href="#" class="bt-menu"><span class="tn-texto">Menú <i class="fa fa-bars" aria-hidden="true"></i></span></a>
				<ul class="ul-menu" style="display: none;">
					<li>
						<a class="inicio" href="index.php"><i class="fa fa-home" aria-hidden="true"></i><br> Inicio</a>
						<a class="inicio" href="nosotros.php"><i class="fa fa-users" aria-hidden="true"></i><br> Nosotros</a>
						<a class="inicio" style="float: right;" href="contacto.php"><i class="fa fa-envelope" aria-hidden="true"></i><br> Contactos</a>
					</li>
					<li><a href="snacks.php">SNACKS</a></li>
					<li><a href="cereales.php">CEREALES</a></li>
					<li><a href="gramineas.php">GRAMÍNEAS</a></li>
					<li><a href="corporativos.php">CORPORATIVOS</a></li>
					<li><a href="#catalogo.php">CATÁLOGO</a></li>
				</ul>
			</div>
		</div>
</div>
    <div class="clear"></div>
		<div class="rod">
		</div>
		<div class="clear"></div>
</header>
