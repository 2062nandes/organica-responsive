<?php require_once 'header.php'; ?>
<body id="inicio">
<?php require('menu.php') ?>
<div class="wrapeverything orswrapper">
	<div class="wrap maincontent">
		<h2 id="snacks" class="tituloProd">Snacks</h2>
		<div class="col100">
      <h3 id="sabrositas">LINEA SABROSITAS  <a href="#linea-sabrositas"> <i class="ojo fa fa-eye" aria-hidden="true"></i></a></h3>
			<ul class="lb-album">
        <li class="col20d">
          <a href="#sabrosita-1">
            <img src="img/productos/papa-frita-sabrositas.png" width="135" height="135" alt="sabrosita01">
            <span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
          </a>
					<p>Papas fritas sabrositas.	</p>
					<div class="lb-overlay " id="sabrosita-1">
					<div class="line-inner">
						<div class="freewrapper1">
			            <div class="coll1">
			              <h2>Papas fritas sabrositas</h2>
			              <img class="lineimg" src="img/productos/papa-frita-sabrositas.png" alt="sabrosita01">
										<a href="#sabrosita-4" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#sabrosita-2" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
			            </div>
			            <div class="coll2 linea-scroll">
			              <h3>LINEA SABROSITAS</h3>
			              <div class="line-links">
			                  <img class="lineimg" src="img/productos/papa-frita-sabrositas.png" alt="producto">
			                </div>
			                  <div class="col-descripcion"><strong>Presentacion</strong>
			                  <p> 230,130 y 20g</p>
			                  <strong>Descripcion</strong>
			                  <p>
			                      Las papas “sabrositas” son obtenidos a partir de tubérculos de papa (Solanum tuberosum) seleccionada que pasa
			      								por un proceso de pelado, rebanado en hojuelas, lavado, fritado en aceite vegetal y adición de sal.
			                  </p>
			            </div>
          			</div>
								<a href="#sabrositas" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
        </li>
        <li class="col20d">
					<a href="#sabrosita-2">
            <img src="img/productos/foto.jpg" width="135" height="135" alt="sabrosita02">
            <span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
          </a>
					<p>Papas fritas sabrositas picantes</p>
					<div class="lb-overlay " id="sabrosita-2">
					<div class="line-inner">
						<div class="freewrapper1">
			            <div class="coll1">
			              <h2>Papas fritas sabrositas picantes</h2>
			              <img class="lineimg" src="img/productos/foto.jpg" alt="sabrosita02">
										<a href="#sabrosita-1" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#sabrosita-3" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
			            </div>
			            <div class="coll2 linea-scroll">
			              <h3>LINEA SABROSITAS</h3>
			              <div class="line-links">
			                  <img class="lineimg" src="img/productos/foto.jpg" alt="producto">
			                </div>
			                  <div class="col-descripcion"><strong>Presentacion</strong>
			                  <p> 20g</p>
			                  <strong>Descripcion</strong>
			                  <p>
			                      Las papas “sabrositas” picantes son obtenidos a partir de tubérculos de papa (Solanum tuberosum) seleccionada que pasa por un proceso de pelado, rebanado en hojuelas, lavado y fritado en aceite vegetal con una posterior adición de sal y colorante rojo picante.
			                  </p>
			            </div>
          			</div>
								<a href="#sabrositas" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
        </li>
        <li class="col20d">
					<a href="#sabrosita-3">
            <img src="img/productos/foto.jpg" width="135" height="135" alt="sabrosita03">
            <span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
          </a>
					<p>Papas hiladas mediana</p>
					<div class="lb-overlay " id="sabrosita-3">
					<div class="line-inner">
						<div class="freewrapper1">
			            <div class="coll1">
			              <h2>Papas hiladas mediana</h2>
			              <img class="lineimg" src="img/productos/foto.jpg" alt="sabrosita03">
										<a href="#sabrosita-2" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#sabrosita-4" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
			            </div>
			            <div class="coll2 linea-scroll">
			              <h3>LINEA SABROSITAS</h3>
			              <div class="line-links">
			                  <img class="lineimg" src="img/productos/foto.jpg" alt="producto">
			                </div>
			                  <div class="col-descripcion"><strong>Presentacion</strong>
			                  <p> 50g</p>
			                  <strong>Descripcion</strong>
			                  <p>
			                      Las papas de corte especial son obtenidos a partir de tubérculos de papa (Solanum tuberosum) seleccionada que pasa por un proceso de pelado, rebanado en forma de finos palitos rectangulares, lavado y fritado en aceite vegetal con una posterior adición de sal.
			                  </p>
			            </div>
          			</div>
								<a href="#sabrositas" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
        </li>
        <li class="col20d">
					<a href="#sabrosita-4">
            <img src="img/productos/manis-salados-horneados.png" width="135" height="135" alt="sabrosita04">
            <span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
          </a>
					<p>Manís salados horneados</p>
					<div class="lb-overlay " id="sabrosita-4">
					<div class="line-inner">
						<div class="freewrapper1">
			            <div class="coll1">
			              <h2>Manís salados horneados</h2>
			              <img class="lineimg" src="img/productos/manis-salados-horneados.png" alt="sabrosita04">
										<a href="#sabrosita-3" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#sabrosita-1" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
			            </div>
			            <div class="coll2 linea-scroll">
			              <h3>LINEA SABROSITAS</h3>
			              <div class="line-links">
			                  <img class="lineimg" src="img/productos/manis-salados-horneados.png" alt="producto">
			                </div>
			                  <div class="col-descripcion"><strong>Presentacion</strong>
			                  <p> 20g</p>
			                  <strong>Descripcion</strong>
			                  <p>
			                      Las papas “sabrositas” picantes son obtenidos a partir de tubérculos de papa (Solanum tuberosum) seleccionada que pasa por un proceso de pelado, rebanado en hojuelas, lavado y fritado en aceite vegetal con una posterior adición de sal y colorante rojo picante.
			                  </p>
			            </div>
          			</div>
								<a href="#sabrositas" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
        </li>
      </ul>
		</div>
<!----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
		<div class="col100">
		<h3 id="fiesta">LINEA FIESTA <a href="#linea-fiesta"> <i class="ojo fa fa-eye" aria-hidden="true"></i></a></h3>
			<ul class="lb-album">
				<li class="col20d">
					<a href="#fiesta-1">
						<img src="img/productos/chesitos-fiesta.png" width="135" height="135" alt="fiesta01">
						<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
					</a>
					<p>Chesitos fiesta</p>
					<div class="lb-overlay " id="fiesta-1">
					<div class="line-inner">
						<div class="freewrapper1">
									<div class="coll1">
										<h2>Chesitos fiesta</h2>
										<img class="lineimg" src="img/productos/chesitos-fiesta.png" alt="fiesta01">
										<a href="#fiesta-6" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#fiesta-2" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
									</div>
									<div class="coll2 linea-scroll">
										<h3>LINEA FIESTA</h3>
										<div class="line-links">
												<img class="lineimg" src="img/productos/chesitos-fiesta.png" alt="producto">
											</div>
												<div class="col-descripcion"><strong>Presentacion</strong>
												<p> </p>
												<strong>Descripcion</strong>
												<p>

												</p>
									</div>
								</div>
								<a href="#fiesta" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
				</li>
				<li class="col20d">
					<a href="#fiesta-2">
						<img src="img/productos/nachos-fiesta-clasico.png" width="135" height="135" alt="fiesta02">
						<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
					</a>
					<p>Nachos fiesta clasico</p>
					<div class="lb-overlay " id="fiesta-2">
					<div class="line-inner">
						<div class="freewrapper1">
									<div class="coll1">
										<h2>Nachos fiesta clasico</h2>
										<img class="lineimg" src="img/productos/nachos-fiesta-clasico.png" alt="fiesta02">
										<a href="#fiesta-1" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#fiesta-4" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
									</div>
									<div class="coll2 linea-scroll">
										<h3>LINEA FIESTA</h3>
										<div class="line-links">
												<img class="lineimg" src="img/productos/nachos-fiesta-clasico.png" alt="producto">
											</div>
												<div class="col-descripcion"><strong>Presentacion</strong>
												<p> 100g 200g</p>
												<strong>Descripcion</strong>
												<p>
														Producto obtenido a base de harinas de maíz y trigo, que pasa por un proceso de extrusión, laminado, cortado en forma de láminas trianuales, fritado y adición de sal.
												</p>
									</div>
								</div>
								<a href="#fiesta" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
				</li>
<!-- Foto-falta
				<li class="col20d">
					<a href="#fiesta-3">
						<img src="img/productos/foto.jpg" width="135" height="135" alt="fiesta03">
						<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
					</a>
					<p>Nachos fiesta picante</p>
					<div class="lb-overlay " id="fiesta-3">
					<div class="line-inner">
						<div class="freewrapper1">
									<div class="coll1">
										<h2>Nachos fiesta picante</h2>
										<img class="lineimg" src="img/productos/foto.jpg" alt="fiesta03">
										<a href="#fiesta-2" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#fiesta-4" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
									</div>
									<div class="coll2 linea-scroll">
										<h3>LINEA FIESTA</h3>
										<div class="line-links">
												<img class="lineimg" src="img/productos/foto.jpg" alt="producto">
											</div>
												<div class="col-descripcion"><strong>Presentacion</strong>
												<p> </p>
												<strong>Descripcion</strong>
												<p>

												</p>
									</div>
								</div>
								<a href="#fiesta" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
				</li>
	Forto falta -->
				<li class="col20d">
					<a href="#fiesta-4">
						<img src="img/productos/nachos-fiestas-cheddar.png" width="135" height="135" alt="fiesta04">
						<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
					</a>
					<p>Nachos fiestas cheddar</p>
					<div class="lb-overlay " id="fiesta-4">
					<div class="line-inner">
						<div class="freewrapper1">
									<div class="coll1">
										<h2>Nachos fiestas cheddar</h2>
										<img class="lineimg" src="img/productos/nachos-fiestas-cheddar.png" alt="fiesta04">
										<a href="#fiesta-2" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#fiesta-5" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
									</div>
									<div class="coll2 linea-scroll">
										<h3>LINEA FIESTA</h3>
										<div class="line-links">
												<img class="lineimg" src="img/productos/nachos-fiestas-cheddar.png" alt="producto">
											</div>
												<div class="col-descripcion"><strong>Presentacion</strong>
												<p> 35g,100g,200g</p>
												<strong>Descripcion</strong>
												<p>
														Producto obtenido a base de harinas de maíz y trigo, que pasa por un proceso de extrusión, laminado, cortado en forma de láminas trianuales, fritado y adición de saborizante de queso cheddar.
												</p>
									</div>
								</div>
								<a href="#fiesta" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
				</li>
				<li class="col20d">
					<a href="#fiesta-5">
						<img src="img/productos/nachos-jalapenios.png" width="135" height="135" alt="fiesta05">
						<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
					</a>
					<p>	Nachos jalapeños</p>
					<div class="lb-overlay " id="fiesta-5">
					<div class="line-inner">
						<div class="freewrapper1">
									<div class="coll1">
										<h2>Nachos jalapeños</h2>
										<img class="lineimg" src="img/productos/nachos-jalapenios.png" alt="fiesta05">
										<a href="#fiesta-4" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#fiesta-6" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
									</div>
									<div class="coll2 linea-scroll">
										<h3>LINEA FIESTA</h3>
										<div class="line-links">
												<img class="lineimg" src="img/productos/nachos-jalapenios.png" alt="producto">
											</div>
												<div class="col-descripcion"><strong>Presentacion</strong>
												<p> 35g,100g,200g</p>
												<strong>Descripcion</strong>
												<p>
													Producto obtenido a base de harinas de maíz y trigo, que pasa por un proceso de extrusión, laminado, cortado en forma de láminas trianuales, fritado y adición de saborizante jalapeño.
												</p>
									</div>
								</div>
								<a href="#fiesta" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
				</li>
				<li class="col20d">
					<a href="#fiesta-6">
						<img src="img\productos\pipocas-fiesta-con-miel.png" width="135" height="135" alt="fiesta06">
						<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
					</a>
					<p>Pipocas fiesta con miel</p>
					<div class="lb-overlay " id="fiesta-6">
					<div class="line-inner">
						<div class="freewrapper1">
									<div class="coll1">
										<h2>Pipocas fiesta con miel</h2>
										<img class="lineimg" src="img\productos\pipocas-fiesta-con-miel.png" alt="fiesta06">
										<a href="#fiesta-5" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#fiesta-1" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
									</div>
									<div class="coll2 linea-scroll">
										<h3>LINEA FIESTA</h3>
										<div class="line-links">
												<img class="lineimg" src="img\productos\pipocas-fiesta-con-miel.png" alt="producto">
											</div>
												<div class="col-descripcion"><strong>Presentacion</strong>
												<p> 20g 200g 400g</p>
												<strong>Descripcion</strong>
												<p>
														Producto obtenido a base del maíz reventón o pisingallo (variedad Zea mays everata Sturt) seleccionado, que pasa por un proceso de expandido y recubierto con miel de caña.
												</p>
									</div>
								</div>
								<a href="#fiesta" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
		<div class="col100">
				<h3 id="gourmet">LINEA GOURMET<a href="#linea-gourmet"> <i class="ojo fa fa-eye" aria-hidden="true"></i></a></h3>
				<ul class="lb-album">
					<li class="col20d">
						<a href="#gourmet-1">
							<img src="img/productos/papas-gourmet-churrasco.png" width="135" height="135" alt="gourmet01">
							<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
						</a>
						<p>Papas gourmet churrasco</p>
						<div class="lb-overlay " id="gourmet-1">
						<div class="line-inner">
							<div class="freewrapper1">
										<div class="coll1">
											<h2>Papas gourmet churrasco</h2>
											<img class="lineimg" src="img/productos/papas-gourmet-churrasco.png" alt="gourmet01">
											<a href="#gourmet-4" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
											<a href="#gourmet-2" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
										</div>
										<div class="coll2 linea-scroll">
											<h3>LINEA GOURMET</h3>
											<div class="line-links">
													<img class="lineimg" src="img/productos/papas-gourmet-churrasco.png" alt="producto">
												</div>
													<div class="col-descripcion"><strong>Presentacion</strong>
													<p> 100g, 300g</p>
													<strong>Descripcion</strong>
													<p>
														Producto obtenido a partir de tubérculos de papa (Solanum tuberosum) seleccionada que pasa por un proceso de pelado, rebanado en hojuelas, lavado y fritado en aceite vegetal con una posterior adición del saborizante churrasco.
													</p>
										</div>
									</div>
									<a href="#gourmet" class="lb-close modal-close"></i></a>
								</div>
							</div>
						</div>
					</li>
					<li class="col20d">
						<a href="#gourmet-2">
							<img src="img/productos/papas-gourmet-kallu.png" width="135" height="135" alt="gourmet02">
							<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
						</a>
						<p>Papas gourmet k'allu</p>
						<div class="lb-overlay " id="gourmet-2">
						<div class="line-inner">
							<div class="freewrapper1">
										<div class="coll1">
											<h2>Papas gourmet k'allu</h2>
											<img class="lineimg" src="img/productos/papas-gourmet-kallu.png" alt="gourmet02">
											<a href="#gourmet-1" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
											<a href="#gourmet-3" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
										</div>
										<div class="coll2 linea-scroll">
											<h3>LINEA GOURMET</h3>
											<div class="line-links">
													<img class="lineimg" src="img/productos/papas-gourmet-kallu.png" alt="producto">
												</div>
													<div class="col-descripcion"><strong>Presentacion</strong>
													<p> 100g, 300g</p>
													<strong>Descripcion</strong>
													<p>
															Producto obtenido a partir de tubérculos de papa (Solanum tuberosum) seleccionada que pasa por un proceso de pelado, rebanado en hojuelas, lavado y fritado en aceite vegetal con una posterior adición del saborizante a k`allu.
													</p>
										</div>
									</div>
									<a href="#gourmet" class="lb-close modal-close"></i></a>
								</div>
							</div>
						</div>
					</li>
					<li class="col20d">
						<a href="#gourmet-3">
							<img src="img/productos/papas-gourmet-cordero-al-palo.png" width="135" height="135" alt="gourmet03">
							<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
						</a>
						<p>Papas gourmet cordero al palo</p>
						<div class="lb-overlay " id="gourmet-3">
						<div class="line-inner">
							<div class="freewrapper1">
										<div class="coll1">
											<h2>Papas gourmet cordero al palo</h2>
											<img class="lineimg" src="img/productos/papas-gourmet-cordero-al-palo.png" alt="gourmet03">
											<a href="#gourmet-2" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
											<a href="#gourmet-4" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
										</div>
										<div class="coll2 linea-scroll">
											<h3>LINEA GOURMET</h3>
											<div class="line-links">
													<img class="lineimg" src="img/productos/papas-gourmet-cordero-al-palo.png" alt="producto">
												</div>
													<div class="col-descripcion"><strong>Presentacion</strong>
													<p> 100g, 300g</p>
													<strong>Descripcion</strong>
													<p>
														Producto obtenido a partir de tubérculos de papa (Solanum tuberosum) seleccionada que pasa por un proceso de pelado, rebanado en hojuelas, lavado y fritado en aceite vegetal con una posterior adición del saborizante cordero al palo.
													</p>
										</div>
									</div>
									<a href="#gourmet" class="lb-close modal-close"></i></a>
								</div>
							</div>
						</div>
					</li>
					<li class="col20d">
						<a href="#gourmet-4">
							<img src="img/productos/papas-gourmet-frutos-de-mar.png" width="135" height="135" alt="gourmet04">
							<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
						</a>
						<p>Papas gourmet frutos de mar</p>
						<div class="lb-overlay " id="gourmet-4">
						<div class="line-inner">
							<div class="freewrapper1">
										<div class="coll1">
											<h2>Papas gourmet frutos de mar</h2>
											<img class="lineimg" src="img/productos/papas-gourmet-frutos-de-mar.png" alt="gourmet04">
											<a href="#gourmet-3" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
											<a href="#gourmet-1" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
										</div>
										<div class="coll2 linea-scroll">
											<h3>LINEA GOURMET</h3>
											<div class="line-links">
													<img class="lineimg" src="img/productos/papas-gourmet-frutos-de-mar.png" alt="producto">
												</div>
													<div class="col-descripcion"><strong>Presentacion</strong>
													<p> 100g, 300g</p>
													<strong>Descripcion</strong>
													<p>
															Producto obtenido a partir de tubérculos de papa (Solanum tuberosum) seleccionada que pasa por un proceso de pelado, rebanado en hojuelas, lavado y fritado en aceite vegetal con una posterior adición del saborizante de frutos de mar.
													</p>
										</div>
									</div>
									<a href="#gourmet" class="lb-close modal-close"></i></a>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
<!--------------------------------------------------------------------------------------------------------------------------------->
<div class="col100">
	<h3 id="recreo">LINEA RECREO <a href="#linea-recreo"> <i class="ojo fa fa-eye" aria-hidden="true"></i></a></h3>
	<ul class="lb-album">
	<!--FOTO LINEA RECREO
		<li class="col20d">
			<a href="#recreo-1">
				<img src="img/productos/foto.jpg" width="135" height="135" alt="recreo01">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Nachos recreo tradicional<p>
			<div class="lb-overlay " id="recreo-1">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Nachos recreo tradicional</h2>
								<img class="lineimg" src="img/productos/foto.jpg" alt="recreo01">
								<a href="#recreo-8" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#recreo-2" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>LINEA RECREO</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/foto.jpg" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> </p>
										<strong>Descripcion</strong>
										<p>

										</p>
							</div>
						</div>
						<a href="#recreo" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
	FOTO LINEA RECREO-->
		<li class="col20d">
			<a href="#recreo-2">
				<img src="img/productos/nachos-recreo-picantes.png" width="135" height="135" alt="recreo02">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Nachos recreo picantes<p>
			<div class="lb-overlay " id="recreo-2">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Nachos recreo picantes</h2>
								<img class="lineimg" src="img/productos/nachos-recreo-picantes.png" alt="recreo02">
								<a href="#recreo-3" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#recreo-3" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>LINEA RECREO</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/nachos-recreo-picantes.png" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> 100g 200g</p>
										<strong>Descripcion</strong>
										<p>
												Producto obtenido a base de harinas de maíz y trigo, que pasa por un proceso de extrusión, laminado, cortado en forma de láminas trianuales, fritado y adición de sal.
										</p>
							</div>
						</div>
						<a href="#recreo" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#recreo-3">
				<img src="img/productos/nachos-recreo-cheddar.png" width="135" height="135" alt="recreo03">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Nachos recreo cheddar<p>
			<div class="lb-overlay " id="recreo-3">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Nachos recreo cheddar</h2>
								<img class="lineimg" src="img/productos/nachos-recreo-cheddar.png" alt="recreo03">
								<a href="#recreo-2" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#recreo-2" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>LINEA RECREO</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/nachos-recreo-cheddar.png" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> </p>
										<strong>Descripcion</strong>
										<p>

										</p>
							</div>
						</div>
						<a href="#recreo" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
		<!--FOTO LINEA RECREO
		<li class="col20d">
			<a href="#recreo-4">
				<img src="img/productos/foto.jpg" width="135" height="135" alt="recreo04">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Pipocas recreo con miel<p>
			<div class="lb-overlay " id="recreo-4">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Pipocas recreo con miel</h2>
								<img class="lineimg" src="img/productos/foto.jpg" alt="recreo04">
								<a href="#recreo-3" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#recreo-5" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>LINEA RECREO</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/foto.jpg" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> </p>
										<strong>Descripcion</strong>
										<p>

										</p>
							</div>
						</div>
						<a href="#recreo" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#recreo-5">
				<img src="img/productos/foto.jpg" width="135" height="135" alt="recreo05">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Papas recreo clásicas<p>
			<div class="lb-overlay " id="recreo-5">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Papas recreo clásicas</h2>
								<img class="lineimg" src="img/productos/foto.jpg" alt="recreo05">
								<a href="#recreo-4" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#recreo-6" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>LINEA RECREO</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/foto.jpg" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> </p>
										<strong>Descripcion</strong>
										<p>

										</p>
							</div>
						</div>
						<a href="#recreo" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#recreo-6">
				<img src="img/productos/foto.jpg" width="135" height="135" alt="recreo06">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Papas recreo picantes<p>
			<div class="lb-overlay " id="recreo-6">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Papas recreo picantes</h2>
								<img class="lineimg" src="img/productos/foto.jpg" alt="recreo06">
								<a href="#recreo-5" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#recreo-7" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>LINEA RECREO</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/foto.jpg" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> </p>
										<strong>Descripcion</strong>
										<p>

										</p>
							</div>
						</div>
						<a href="#recreo" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#recreo-7">
				<img src="img/productos/foto.jpg" width="135" height="135" alt="recreo01">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Papas recreo limón<p>
			<div class="lb-overlay " id="recreo-7">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Papas recreo limón</h2>
								<img class="lineimg" src="img/productos/foto.jpg" alt="recreo07">
								<a href="#recreo-6" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#recreo-8" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>LINEA RECREO</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/foto.jpg" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> </p>
										<strong>Descripcion</strong>
										<p>

										</p>
							</div>
						</div>
						<a href="#recreo" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#recreo-8">
				<img src="img/productos/foto.jpg" width="135" height="135" alt="recreo01">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Papas recreo cheddar<p>
			<div class="lb-overlay " id="recreo-8">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Papas recreo cheddar</h2>
								<img class="lineimg" src="img/productos/foto.jpg" alt="recreo08">
								<a href="#recreo-7" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#recreo-1" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>LINEA RECREO</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/foto.jpg" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> </p>
										<strong>Descripcion</strong>
										<p>

										</p>
							</div>
						</div>
						<a href="#recreo" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
	</ul>
</div>
FOTO LINEA RECREO -->
<!------------------------------------------------------------------------------------------------->
<div class="col100">
		<h3 id="tossitos">LINEA TOSSITOS<a href="#linea-tossitos"> <i class="ojo fa fa-eye" aria-hidden="true"></i></a></h3>
		<ul class="lb-album">
			<li class="col20d">
				<a href="#tossito-1">
					<img src="img/productos/tossitos-pasankallas.png" width="135" height="135" alt="tossito01">
					<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
				</a>
				<p>Tossitos Pasank'allas</p>
				<div class="lb-overlay " id="tossito-1">
				<div class="line-inner">
					<div class="freewrapper1">
								<div class="coll1">
									<h2>TOSSITOS PASANK'ALLAS</h2>
									<img class="lineimg" src="img/productos/tossitos-pasankallas.png" alt="tossito01">
									<a href="#tossito-1" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
									<a href="#tossito-1" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
								</div>
								<div class="coll2 linea-scroll">
									<h3>LINEA TOSSITOS</h3>
									<div class="line-links">
											<img class="lineimg" src="img/productos/tossitos-pasankallas.png" alt="producto">
										</div>
											<div class="col-descripcion"><strong>Presentacion</strong>
											<p> 30g y 150g</p>
											<strong>Descripcion</strong>
											<p>
												Los Tossitos son obtenidos a partir de maíz pelado y desgerminado, sometido a un proceso de expandido y saborizado.
											</p>
								</div>
							</div>
							<a href="#tossitos" class="lb-close modal-close"></i></a>
						</div>
					</div>
				</div>
			</li>
		</ul>
			<?php require('linea.php'); ?>
	</div>
</div>
</div>
<div class="clear"></div>
<?php require_once 'footer.php'; ?>
