<?php
    date_default_timezone_set("America/La_Paz");
    header('content-type: text/html; charset: utf-8');
    $shora=date("H:i:s");
    $sfecha=date("d/m/Y");
    $ip=$_SERVER['REMOTE_ADDR'];
    //nombres
    $fecha = @trim(stripslashes($_POST['fecha']));
    $pnombre = @trim(stripslashes($_POST['pnombre']));
    $snombre = @trim(stripslashes($_POST['snombre']));
    $papellido = @trim(stripslashes($_POST['papellido']));
    $sapellido = @trim(stripslashes($_POST['sapellido']));

    //fecha nacimiento
    $dia = @trim(stripslashes($_POST['dia']));
    $mes = @trim(stripslashes($_POST['mes']));
    $anio = @trim(stripslashes($_POST['anio']));
    $sex = @trim(stripslashes($_POST['sex']));
    $estadocivil = @trim(stripslashes($_POST['estadocivil']));

    //nro contacto
    $casa = @trim(stripslashes($_POST['casa']));
    $oficina = @trim(stripslashes($_POST['oficina']));
    $celular = @trim(stripslashes($_POST['celular']));
    $email = @trim(stripslashes($_POST['email']));

    $avcalle = @trim(stripslashes($_POST['avcalle']));
    $ncasa = @trim(stripslashes($_POST['ncasa']));
    $ciudad = @trim(stripslashes($_POST['ciudad']));
    $depa = @trim(stripslashes($_POST['depa']));
    $pais = @trim(stripslashes($_POST['pais']));
    $casilla = @trim(stripslashes($_POST['casilla']));

    $casilla = @trim(stripslashes($_POST['casilla']));
    $ci = @trim(stripslashes($_POST['ci']));
    $nacionalidad = @trim(stripslashes($_POST['nacionalidad']));
    $nit = @trim(stripslashes($_POST['nit']));
    $cnombre = @trim(stripslashes($_POST['cnombre']));
    $cdia = @trim(stripslashes($_POST['cdia']));
    $cmes = @trim(stripslashes($_POST['cmes']));
    $canio = @trim(stripslashes($_POST['canio']));
    $cci = @trim(stripslashes($_POST['cci']));

    $bnombre = @trim(stripslashes($_POST['bnombre']));
    $brel = @trim(stripslashes($_POST['brel']));
    $bci = @trim(stripslashes($_POST['bci']));
    $patnombre = @trim(stripslashes($_POST['patnombre']));

    $codpat = @trim(stripslashes($_POST['codpat']));
    $pcasa = @trim(stripslashes($_POST['pcasa']));
    $pcel = @trim(stripslashes($_POST['pcel']));
    $pemail = @trim(stripslashes($_POST['pemail']));

    $comision = @trim(stripslashes($_POST['comision']));
    $banco = @trim(stripslashes($_POST['banco']));
    $cuenta = @trim(stripslashes($_POST['cuenta']));
    $garantizo = @trim(stripslashes($_POST['garantizo']));

    $nombre = @trim(stripslashes($_POST['nombre']));
    $telefono = @trim(stripslashes($_POST['telefono']));
    $movil = @trim(stripslashes($_POST['movil']));
    $direccion = @trim(stripslashes($_POST['direccion']));
    $ciudad = @trim(stripslashes($_POST['ciudad']));
    $mensaje = @trim(stripslashes($_POST['mensaje']));
    $email_to = @trim(stripslashes($_POST['area']));

    $email_to = "nandes.ingsistemas@hotmail.com";

    //only for testing:
    //if($email_to = "santa"){
    //    $email_to = "apaza.alcides@gmail.com";
    //}else if($email_to = "cocha"){
    //    $email_to = "headstrong.hdst@gmail.com";
    //}

    if($_POST['email'] ==""){
        $email_from = "organicadelsur.com";
    }
    $subject = "SOLICITUD DE REGISTRO desde la web organicadelsur.com";

    $body = "<b>INFORMACIÓN PERSONAL</b><br>";
    $body .= 'Fecha: ' .$fecha."<br>";
    $body .= 'Primer nombre: ' .$pnombre."<br>";
    $body .= 'Segundo nombre: ' .$snombre."<br>";
    $body .= '1er apellido: ' .$papellido."<br>";
    $body .= '2do Apellido: ' .$sapellido."<br>";

    $body .= "<br><b>FECHA DE NACIMIENTO</b><br>";
    $body .= 'Día: ' .$dia."<br>";
    $body .= 'Mes: ' .$mes."<br>";
    $body .= 'Año: ' .$anio."<br>";
    $body .= 'Sexo: ' .$sex."<br>";
    $body .= 'Estado Civil: ' .$estadocivil."<br>";

    $body .= "<br><b>NÚMERO DE CONTACTO</b><br>";
    $body .= 'Casa: ' .$casa."<br>";
    $body .= 'Oficina: ' .$oficina."<br>";
    $body .= 'Celular: ' .$celular."<br>";
    $body .= 'E-mail: ' .$email."<br>";

    $body .= "<br><b>DIRECCIÓN</b><br>";
    $body .= 'Avenida/Calle: ' .$avcalle."<br>";
    $body .= 'Nº de casa: ' .$ncasa."<br>";
    $body .= 'Ciudad: ' .$ciudad."<br>";
    $body .= 'Departamento: ' .$depa."<br>";
    $body .= 'País: ' .$pais."<br>";
    $body .= 'Casilla: ' .$casilla."<br>";
    $body .= 'Carnet de Identidad C.I.: ' .$ci."<br>";
    $body .= 'Nacionalidad: ' .$nacionalidad."<br>";
    $body .= 'NIT: ' .$nit."<br>";

    $body .= "<br><b>DATOS DEL CÓNYUGE</b><br>";
    $body .= 'Nombre del cónyuge(apellidos y nombres): ' .$cnombre."<br>";
    $body .= "<b>Nacimiento del cónyuge: </b><br>";
    $body .= 'Día: ' .$cdia."<br>";
    $body .= 'Mes: ' .$cmes."<br>";
    $body .= 'Año: ' .$canio."<br>";
    $body .= 'C.I.: ' .$cci."<br>";

    $body .= "<br><b>DATOS DEL BENEFICIARIO</b><br>";
    $body .= 'Apellidos y Nombres: ' .$bnombre."<br>";
    $body .= 'Relación: ' .$brel."<br>";
    $body .= 'C.I.: ' .$bci."<br>";

    $body .= "<br><b>INFORMACIÓN DEL PATROCINADOR</b><br>";
    $body .= 'Apellidos y Nombres: ' .$patnombre."<br>";
    $body .= 'Código de Patrocinador: ' .$codpat."<br>";
    $body .= 'Casa: ' .$pcasa."<br>";
    $body .= 'Celular: ' .$pcel."<br>";
    $body .= 'Dirección E-mail: ' .$pemail."<br>";

    $body .= "<br><b>INFORMACIÓN PARA EL PAGO DE LA COMISIÓN</b><br>";
    if ($comision == "bcomision"){
        $body .= "<b>Se escogió la comisión por Cuenta de Banco<b><br>";
        $body .= 'Nombre de banco: ' .$banco."<br>";
        $body .= 'Cuenta Nº:' .$cuenta."<br>";
    }else{
        $body .= "<b>Se escogió la comisión por DXN Filial Bolivia (a través de contra entrega de productos)<b><br>";
    }
    $body .= '==================================='."<br>";
    $body .= 'Hora y fecha de envío: El '.$sfecha.' a las '.$shora."<br>";
    $body .= 'IP del remitente: '.$ip;

    $headers = 'From: '.$email_from."\r\n";
    $headers .= 'MIME-Version: 1.0'."\r\n";
    $headers .= 'Content-Type: text/HTML; charset=utf-8' ."\r\n";

    if( mail($email_to, $subject, $body, $headers) ){
        echo "enviado";
    }
    else{
        echo "nope";
    }

    // Return an appropriate response to the browser TRUE on sending, FALSE if not
?>
