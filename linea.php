<!--
<body id="inicio">
<div id="fakebg"><div class="circle"><div class="inner"></div> <div class="load-container load3"> <div class="loader">Cargando...</div></div></div></div>
-->
<ul class="lb-album">
        <li>
          <div class="lb-overlay " id="linea-sabrositas">
					<div class="line-inner">
						<div class="freewrapper1">
			            <div class="coll1">
			              <h2>LINEA SABROSITAS</h2>
			              <img class="lineimge" src="img/snacks.jpg" alt="snacks">
										<a href="#linea-personalizada" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#linea-fiesta" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
			            </div>
			            <div class="coll2 linea-scroll">
                    <h3>LINEA SABROSITAS</h3>
                    <div class="line-links">
                      <a href="snacks.php#sabrosita-1">- Papas fritas
                      <!--<img class="prod-imge" src="img/productos/papa-frita-sabrositas.png" alt="producto">
                    --></a>
                      <a href="snacks.php#sabrosita-2">- Papas fritas sabrositas picantes
                      <!--<img class="prod-imge" src="img/productos/foto.jpg" alt="producto">
                    --></a>
                      <a href="snacks.php#sabrosita-3">- Papas hiladas mediana
                      <!--<img class="prod-imge" src="img/productos/foto.jpg" alt="producto">
                    --></a>
                      <a href="snacks.php#sabrosita-4">- Manís salados horneados
                      <!--<img class="prod-imge" src="img/productos/manis-salados-horneados.png" alt="producto">
                    --></a>
                  </div>
			            </div>
          			</div>
								<a href="#Linea" class="lb-close modal-close"></i></a>
							</div>
					</div>
        </li>
        <li>
          <div class="lb-overlay " id="linea-fiesta">
          <div class="line-inner">
            <div class="freewrapper1">
                  <div class="coll1">
                    <h2>LINEA FIESTA</h2>
                    <img class="lineimge" src="img/snacks.jpg" alt="snacks">
                    <a href="#linea-sabrositas" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
                    <a href="#linea-gourmet" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="coll2 linea-scroll">
                    <h3>LINEA FIESTA</h3>
                    <div class="line-links">
                      <a href="snacks.php#fiesta-1">Chesitos fiesta
                        <!--<img class="prod-imge" src="img/foto.jpg" alt="producto">
                      --></a>
                      <a href="snacks.php#fiesta-2">Nachos fiesta clasico
                        <!--<img class="prod-imge" src="img/productos/nachos-fiesta-clasico.png" alt="producto">
                      --></a>
                      <a href="snacks.php#fiesta-3">Nachos fiestas picantes
                        <!--<img class="prod-imge" src="img/foto.jpg" alt="producto">
                      --></a>
                      <a href="snacks.php#fiesta-4">Nachos fiestas cheddar
                        <!--<img class="prod-imge" src="img/productos/nachos-fiestas-cheddar.png" alt="producto">
                      --></a>
                      <a href="snacks.php#fiesta-5">Nachos jalapeños
                        <!--<img class="prod-imge" src="img/productos/nachos-jalapenios.png" alt="producto">
                      --></a>
                      <a href="snacks.php#fiesta-6">Pipocas fiesta con miel
                        <!--<img class="prod-imge" src="img/productos/pipocas-fiesta-con-miel.png" alt="producto">
                      --></a>
                  </div>
                  </div>
                </div>
                <a href="#Linea" class="lb-close modal-close"></i></a>
              </div>
          </div>
        </li>
        <li>
          <div class="lb-overlay " id="linea-gourmet">
          <div class="line-inner">
            <div class="freewrapper1">
                  <div class="coll1">
                    <h2>LINEA GOURMET</h2>
                    <img class="lineimge" src="img/snacks.jpg" alt="snacks">
                    <a href="#linea-fiesta" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
                    <a href="#linea-recreo" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="coll2 linea-scroll">
                    <h3>LINEA GOURMET</h3>
                    <div class="line-links">
                      <a href="snacks.php#gourmet-1">Papas gourmet churrasco
                        <!--<img class="prod-imge" src="img/productos/papas-gourmet-churrasco.png" alt="producto">
                      --></a>
                      <a href="snacks.php#gourmet-2">Papas gourmet k'allu
                        <!--<img class="prod-imge" src="img/productos/papas-gourmet-kallu.png" alt="producto">
                      --></a>
                      <a href="snacks.php#gourmet-3">Papas gourmet cordero al palo
                        <!--<img class="prod-imge" src="img/productos/papas-gourmet-cordero-al-palo.png" alt="producto">
                      --></a>
                      <a href="snacks.php#gourmet-4">Papas gourmet frutos de mar
                        <!--<img class="prod-imge" src="img/productos/papas-gourmet-frutos-de-mar.png" alt="producto">
                      --></a>
                  </div>
                  </div>
                </div>
                <a href="#Linea" class="lb-close modal-close"></i></a>
              </div>
          </div>
        </li>
        <li>
          <div class="lb-overlay " id="linea-recreo">
          <div class="line-inner">
            <div class="freewrapper1">
                  <div class="coll1">
                    <h2>LINEA RECREO</h2>
                    <img class="lineimge" src="img/snacks.jpg" alt="snacks">
                    <a href="#linea-gourmet" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
                    <a href="#linea-tossitos" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="coll2 linea-scroll">
                    <h3>LINEA RECREO</h3>
                    <div class="line-links">
                    <!--  <a href="snacks.php#recreo-1">Nachos recreo tradicional</a>-->
                      <a href="snacks.php#recreo-2">Nachos recreo picantes</a>
                      <a href="snacks.php#recreo-3">Nachos recreo cheddar</a>
                    <!--
                      <a href="snacks.php#recreo-4">Pipocas recreo con miel</a>
                      <a href="snacks.php#recreo-5">Papas recreo clásicas</a>
                      <a href="snacks.php#recreo-6">Papas recreo picantes</a>
                      <a href="snacks.php#recreo-7">Papas recreo limón</a>
                      <a href="snacks.php#recreo-8">Papas recreo cheddar</a>
                    -->  
                  </div>
                  </div>
                </div>
                <a href="#Linea" class="lb-close modal-close"></i></a>
              </div>
          </div>
        </li>
        <li>
          <div class="lb-overlay " id="linea-tossitos">
          <div class="line-inner">
            <div class="freewrapper1">
                  <div class="coll1">
                    <h2>LINEA TOSSITOS</h2>
                    <img class="lineimge" src="img/snacks.jpg" alt="snacks">
                    <a href="#linea-recreo" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
                    <a href="#linea-c-real" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="coll2 linea-scroll">
                    <h3>LINEA TOSSITOS</h3>
                    <div class="line-links">
                      <a href="snacks.php#tossito-1">Tossitos pasank'allas
                        <!--<img class="prod-imge" src="img/productos/tossitos-pasankallas.png" alt="producto">
                      --></a>
                  </div>
                  </div>
                </div>
                <a href="#Linea" class="lb-close modal-close"></i></a>
              </div>
          </div>
        </li>
        <li>
          <div class="lb-overlay " id="linea-c-real">
          <div class="line-inner">
            <div class="freewrapper1">
                  <div class="coll1">
                    <h2>LINEA C-REAL</h2>
                    <img class="lineimge" src="img/snacks.jpg" alt="snacks">
                    <a href="#linea-tossitos" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
                    <a href="#linea-sr-maiz" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="coll2 linea-scroll">
                    <h3>LINEA C-REAL</h3>
                    <div class="line-links">
                      <a href="cereales.php#c-real-1">Aritos de frutas
                        <!--<img class="prod-imge" src="img/productos/aritos-de-frutas.png" alt="producto">
                      --></a>
                      <a href="cereales.php#c-real-2">Bolitas de chocolates
                        <!--<img class="prod-imge" src="img/productos/bolitas-de-chocolate.png" alt="producto">
                      --></a>
                  </div>
                  </div>
                </div>
                <a href="#Linea" class="lb-close modal-close"></i></a>
              </div>
          </div>
        </li>
        <li>
          <div class="lb-overlay " id="linea-sr-maiz">
          <div class="line-inner">
            <div class="freewrapper1">
                  <div class="coll1">
                    <h2>LINEA SR-MAIZ</h2>
                    <img class="lineimge" src="img/snacks.jpg" alt="snacks">
                    <a href="#linea-c-real" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
                    <a href="#linea-en-grano" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="coll2 linea-scroll">
                    <h3>LINEA SR-MAIZ</h3>
                    <div class="line-links">
                      <a href="cereales.php#sr-maiz-1">Maíz Sr. Maíz cebolla taper
                        <!--<img class="prod-imge" src="img/productos/maiz-sr-maiz-cebolla-taper.png" alt="producto">
                      --></a>
                      <a href="cereales.php#sr-maiz-2">Maní con pasas zipper
                        <!--<img class="prod-imge" src="img/productos/mani-con-pasas.png" alt="producto">
                      --></a>
                  </div>
                  </div>
                </div>
                <a href="#Linea" class="lb-close modal-close"></i></a>
              </div>
          </div>
        </li>
        <li>
          <div class="lb-overlay " id="linea-en-grano">
          <div class="line-inner">
            <div class="freewrapper1">
                  <div class="coll1">
                    <h2>EN GRANO</h2>
                    <img class="lineimge" src="img/snacks.jpg" alt="snacks">
                    <a href="#linea-sr-maiz" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
                    <a href="#linea-molidos" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="coll2 linea-scroll">
                    <h3>EN GRANO</h3>
                    <div class="line-links">
                      <a href="gramineas.php#en-grano-1">Arroz
                        <!--<img class="prod-imge" src="img/productos/arroz.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#en-grano-2">Azúcar impalpable
                        <!--<img class="prod-imge" src="img/productos/azucar-impalpable.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#en-grano-3">Garbanzos
                        <!--<img class="prod-imge" src="img/productos/garbanzos.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#en-grano-4">Lentejas
                        <!--<img class="prod-imge" src="img/productos/lentejas.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#en-grano-5">Trigos enteros
                        <!--<img class="prod-imge" src="img/productos/trigos-enteros.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#en-grano-6">Trigos partidos
                        <!--<img class="prod-imge" src="img/productos/trigos-partidos.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#en-grano-7">Maíz para somo
                        <!--<img class="prod-imge" src="img/productos/foto.jpg" alt="producto">
                      --></a>
                      <a href="gramineas.php#en-grano-8">Maíz pipocas
                        <!--<img class="prod-imge" src="img/productos/maiz-pipocas.png" alt="producto">
                      --></a>
                  </div>
                  </div>
                </div>
                <a href="#Linea" class="lb-close modal-close"></i></a>
              </div>
          </div>
        </li>
        <li>
          <div class="lb-overlay " id="linea-molidos">
          <div class="line-inner">
            <div class="freewrapper1">
                  <div class="coll1">
                    <h2>MOLIDOS</h2>
                    <img class="lineimge" src="img/snacks.jpg" alt="snacks">
                    <a href="#linea-en-grano" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
                    <a href="#linea-deshidratados" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="coll2 linea-scroll">
                    <h3>MOLIDOS</h3>
                    <div class="line-links">
                      <a href="gramineas.php#molidos-1">Harina blanca
                        <!--<img class="prod-imge" src="img/productos/harina-blanca.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#molidos-2">Lawas de choclos
                        <!--<img class="prod-imge" src="img/productos/lawa-de-choclo.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#molidos-3">Lawas de chunos
                        <!--<img class="prod-imge" src="img/productos/lawas-de-chunio.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#molidos-4">Linazas molidas
                        <!--<img class="prod-imge" src="img/productos/linazas-molidas.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#molidos-5">Panes duros
                        <!--<img class="prod-imge" src="img/productos/pan-duro.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#molidos-6">Sémolas blancas
                        <!--<img class="prod-imge" src="img/productos/foto.jpg" alt="producto">
                      --></a>
                      <a href="gramineas.php#molidos-7">Tojoris
                        <!--<img class="prod-imge" src="img/productos/foto.jpg" alt="producto">
                      --></a>
                      <a href="gramineas.php#molidos-8">Avenas de sopa
                        <!--<img class="prod-imge" src="img/productos/foto.jpg" alt="producto">
                      --></a>
                      <a href="gramineas.php#molidos-9">Jankaquipas
                        <!--<img class="prod-imge" src="img/productos/jankaquipa.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#molidos-10">Cocos rallados
                        <!--<img class="prod-imge" src="img/productos/coco-rallado.png" alt="producto">
                      --></a>
                  </div>
                  </div>
                </div>
                <a href="#Linea" class="lb-close modal-close"></i></a>
              </div>
          </div>
        </li>
        <li>
          <div class="lb-overlay " id="linea-deshidratados">
          <div class="line-inner">
            <div class="freewrapper1">
                  <div class="coll1">
                    <h2>DESHIDRATADOS</h2>
                    <img class="lineimge" src="img/snacks.jpg" alt="snacks">
                    <a href="#linea-molidos" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
                    <a href="#linea-semillas" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="coll2 linea-scroll">
                    <h3>DESHIDRATADOS</h3>
                    <div class="line-links">
                      <a href="gramineas.php#deshidratado-1">Chuños negros
                        <!--<img class="prod-imge" src="img/productos/chunios-negros.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#deshidratado-2">Mocochinchis
                        <!--<img class="prod-imge" src="img/productos/mocochinchi.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#deshidratado-3">Tuntas
                        <!--<img class="prod-imge" src="img/productos/tuntas.png" alt="producto">
                      --></a>
                  </div>
                  </div>
                </div>
                <a href="#Linea" class="lb-close modal-close"></i></a>
              </div>
          </div>
        </li>
        <li>
          <div class="lb-overlay " id="linea-semillas">
          <div class="line-inner">
            <div class="freewrapper1">
                  <div class="coll1">
                    <h2>SEMILLAS</h2>
                    <img class="lineimge" src="img/snacks.jpg" alt="snacks">
                    <a href="#linea-deshidratados" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
                    <a href="#linea-personalizada" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="coll2 linea-scroll">
                    <h3>SEMILLAS</h3>
                    <div class="line-links">
                      <a href="gramineas.php#semilla-1">Manís con cáscara
                        <!--<img class="prod-imge" src="img/productos/mani-con-cascara.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#semilla-2">Manís pelados
                        <!--<img class="prod-imge" src="img/productos/mani-pelado.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#semilla-3">Porotos blancos
                        <!--<img class="prod-imge" src="img/productos/porotos-blancos.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#semilla-4">Porotos negros
                        <!--<img class="prod-imge" src="img/productos/porotos-negros.png" alt="producto">
                      --></a>
                      <a href="gramineas.php#semilla-5">Porotos rojos
                        <!--<img class="prod-imge" src="img/productos/porotos-rojos.png" alt="producto">
                      --></a>
                  </div>
                  </div>
                </div>
                <a href="#Linea" class="lb-close modal-close"></i></a>
              </div>
          </div>
        </li>
        <li>
          <div class="lb-overlay " id="linea-personalizada">
          <div class="line-inner">
            <div class="freewrapper1">
                  <div class="coll1">
                    <h2>LINEA PERSONALIZADA</h2>
                    <img class="lineimge" src="img/snacks.jpg" alt="snacks">
                    <a href="#linea-semillas" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
                    <a href="#linea-sabrositas" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="coll2 linea-scroll">
                    <h3>LINEA PERSONALIZADA</h3>
                    <div class="line-links">
                      <a href="corporativos.php#personalizada-1">Papas Bolívar
                        <!--<img class="prod-imge" src="img/productos/papas-bolivar.png" alt="producto">
                      --></a>
                      <a href="corporativos.php#personalizada-2">Papas Wilsterman
                        <!--<img class="prod-imge" src="img/productos/papas-wilsterman.png" alt="producto">
                      --></a>
                  </div>
                  </div>
                </div>
                <a href="#Linea" class="lb-close modal-close"></i></a>
              </div>
          </div>
        </li>
<!--        <li>
          <div class="lb-overlay " id="catalogo">
          <div class="line-inner">
            <div class="freewrapper1">
                  <div class="coll1">
                    <h2>LINEA </h2>
                    <img class="lineimge" src="img/snacks.jpg" alt="snacks">
                    <a href="#linea-personalizada" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
                    <a href="#linea-fiesta" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  </div>
                  <div class="coll2 linea-scroll">
                    <h3>LINEA </h3>
                    <div class="line-links">

                  </div>
                  </div>
                </div>
                <a href="#Linea" class="lb-close modal-close"></i></a>
              </div>
          </div>
        </li>
      -->
</ul>
