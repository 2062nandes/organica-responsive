  $(document).ready(function(){
    $('#ir-arriba').hide();
    $(function(){
      $(window).scroll(function(){
        if ($(this).scrollTop() > 250) {
            $('#ir-arriba').fadeIn();
        }else{
            $('#ir-arriba').fadeOut();
        }
      })
    })
    $('.contenedor-menu').click(function(){
      $(this).children('.ul-menu').slideToggle();
    });
  });

$(function(){
  //por-defecto
   $('#contenido-mapa').load('mapas/cochabamba.html');
  //lista de mapas
   $('#santa-cruz').click(function(event){
     $('#contenido-mapa').load('mapas/santa-cruz.html');
   });
   $("#la-paz").click(function(event) {
      $("#contenido-mapa").load('mapas/la-paz.html');
   });
   $("#oruro").click(function(event) {
     $("#contenido-mapa").load('mapas/oruro.html');
   });
   $("#potosi").click(function(event) {
      $("#contenido-mapa").load('mapas/potosi.html');
   });
   $("#riberalta").click(function(event) {
       $("#contenido-mapa").load('mapas/riberalta.html');
   });
   $("#sucre").click(function(event) {
      $("#contenido-mapa").load('mapas/sucre.html');
   });
   $("#tarija").click(function(event) {
      $("#contenido-mapa").load('mapas/tarija.html');
   });
   $("#trinidad").click(function(event) {
      $("#contenido-mapa").load('mapas/trinidad.html');
   });
   $("#cochabamba").click(function(event) {
      $("#contenido-mapa").load('mapas/cochabamba.html');
  });
});
