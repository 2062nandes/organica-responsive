var thedelay;

//dropdown
function init_dropdown() {
  // Does element exist?
  if (!$('div.dropdown').length) {
    // If not, exit.
    return;
  }
  // Add listener for hover.
  $('.dropdown').hover(function() {
    // Show subsequent <ul>.
    $(this).find('.inner').fadeIn(1, function(){
      $(this).find('.inner').slideUp(0);
      $(this).find('.inner').slideDown(0);
    });
    console.log("hover captured");
  }
  ,
  function() {
    // Hide subsequent <ul>.
    $(this).find('.inner').slideUp(0, function(){
      $(this).find('.inner').hide();
    });
  });
}
$(document).ready(function(){

    function rotateLogo(){
        if($(".logoimg").hasClass("flipped"))
            $(".logoimg").removeClass("flipped");
        else
            $(".logoimg").addClass("flipped");
    };

    setInterval(rotateLogo, 6000);

 init_dropdown();

  function replace(selector){
    texto = $(selector).html().trim();
    newTexto = "";
    for (var i = 0; i <texto.length; i++){
      newTexto += "<span>"+texto[i]+"</span>";
    }
    $(selector).html("");
    $(selector).append(newTexto);
  }
  replace(".fiesta");
  replace(".fiesta2");

  //start of loader styles
  function starterAnim(){
      $("#fakebg").addClass('loadedb');
      setTimeout(function(){ $("#fakebg").css("display", "none") }, 550);
      thedelay = 550;

      $('.ifiesta').delay(thedelay).animate({opacity: '1', left: '0'}, 550);
      thedelay +=650;

      //botones nav
      for (var i=1; i<=$(".topsocial ul li").length; i++){
        $('.topsocial ul li:nth-child('+i+')').delay(thedelay).animate({opacity: '1', top: '0'});
        thedelay+=80;
      }
      thedelay+= 300;
      //botones topnav
      for (var i=1; i<=$(".topnav ul li").length; i++){
        $('.topnav ul li:nth-child('+i+')').delay(thedelay).animate({opacity: '1', top: '0'});
        thedelay+=80;
      }
      thedelay+=300;
      //rubro
      for (var i=1; i<=$("nav .dropdown").length; i++){
        $('nav .dropdown:nth-child('+i+')').delay(thedelay).animate({opacity: '1'});
        thedelay+=150;
      }
      thedelay+300;
      $('.tierra').delay(thedelay).animate({opacity: '1'});
      $('.logoimg').delay(thedelay).animate({opacity: '1'});
      thedelay+=100;

      $(".themap").attr("src", $(".themap").attr("data-isrc"));
  }

  /*
  <img class="ifiesta logoimg" src="img/01-rganica.png" alt="Fiesta">
  <img style="display:none;" src="img/02-mundo.png" alt="" />
  <img style="display:none;" src="img/03-cuerpo-mundo.png" alt="" />
  <img style="display:none;" src="img/04-btn-contact.png" alt="" />
  <img style="display:none;" src="img/05-btn-home.png" alt="" />
  <img style="display:none;" src="img/06-btn-us.png" alt="" />
  <img style="display:none;" src="img/07-btn-plus.png" alt=""/>
  <img style="display:none;" src="img/08-btn-tube.png" alt="" />
  <img style="display:none;" src="img/09-btn-face.png" alt=""/>
  <img style="display:none;" src="img/10-globito.png" alt="" />
  */
 var tover = ['01-rganica.png','02-mundo.png','03-cuerpo-mundo.png']; //just
  all = tover.length;
  var counter = 0;
  for ( var i=0; i < all; i++){
    var img = new Image();
    img.onload = function(){
      counter+=1;
      console.log($(this).attr('src')+' - cargada!');
      console.log("inner counter"+counter);
      if (counter == all){
        starterAnim();
        console.log("now is all charged");
      }
    }
    img.src="img/"+tover[i]; //here we define the path
  }
  console.log("all"+all);

  //end of loader styles

    //form
      $(function(){

        $('input, textarea').each(function() {
          $(this).on('focus', function() {
            $(this).parent('.input').addClass('active');
         });

        if($(this).val() != '') $(this).parent('.input').addClass('active');
        });
      });
      var message = $('#statusMessage');
      $('.deletebtn').click(function() {
        message.removeClass("animMessage");
        $("#theForm input").css("box-shadow", "none");
        $("#theForm textarea").css("box-shadow", "none");
        $("#theForm select").css("box-shadow", "none");
        $(".input").removeClass("active");
      });

      $('.submitbtn').click(function() {
        message.removeClass("animMessage");
        $("#theForm input").css("box-shadow", "");
        $("#theForm textarea").css("box-shadow", "");
        $("#theForm select").css("box-shadow", "");
        if($("#theForm")[0].checkValidity()){
          $.post("mail.php", $("#theForm").serialize(), function(response) {
            if (response == "enviado"){
              $("#theForm")[0].reset();
              $(".input").removeClass("active");
              message.html("Su mensaje fue envíado correctamente,<br>le responderemos en breve");
            }
            else{
              message.html("Su mensaje no pudo ser envíado");
            }
            message.addClass("animMessage");
            $("#theForm input").css("box-shadow", "none");
            $("#theForm textarea").css("box-shadow", "none");
            $("#theForm select").css("box-shadow", "none");
          });
          return false;
        }
      });

      //
      var message = $('#statusReg');
      $('.deleteregbtn').click(function() {
        message.removeClass("animMessage");
        $("#theReg input").css("box-shadow", "none");
        $("#theReg textarea").css("box-shadow", "none");
        $("#theReg select").css("box-shadow", "none");
        //$("#theReg .input").removeClass("active");
        message.html("")
      });

      $('.submitregbtn').click(function() {
        message.removeClass("animMessage");
        //$("#theReg input").css("box-shadow", "");
        //$("#theReg textarea").css("box-shadow", "");
        //$("#theReg select").css("box-shadow", "");
        if($("#theReg")[0].checkValidity()){

          $.post("reg.php", $("#theReg").serialize(), function(response) {
            if (response == "enviado"){
              $("#theReg")[0].reset();
              $("#theReg .input").removeClass("active");
              message.html("Su solicitud de registro fue envíada correctamente,<br>le responderemos en breve");
            }
            else{
              message.html("Su solicitud no pudo ser envíada");
            }
            message.addClass("animMessage");
            $("#theReg input").css("box-shadow", "none");
            $("#theReg textarea").css("box-shadow", "none");
            $("#theReg select").css("box-shadow", "none");
          });
          return false;
        }else{
          $("header").css("z-index", "-1");
          setTimeout(
            function(){
              $("header").css("z-index", "90");
            }, 5000
            );
        }
      });

      //aftermap
      $(".ontop").click(function(){
        $(this).hide();
      });


    //jump
    //var scrollToPosition = $scrollToElement.position().top; es para un obtener una posición relativa al contenedor
    //var scrollToPosition = $scrollToElement.offset().top; es para obtener la posición relativa del elemento a toda la página

    $(function() {
        $(".jlink").click(function() {
          if (this.hash || this.data("jlink")) {
            var hash = "";
            if (this.hash){
              hash = this.hash.substr(1);
            }else if(this.data("jlink")){
              hash = this.data("jlink");
            }
            console.log("hash:"+hash);
            var $scrollToElement;
            $scrollToElement = $("*[id=" + hash + "]");

            //var headerHeight = $('header').height();
            headerHeight = 108; //lo he hardcodeado para que no hayan problemas
            //console.log("headerHeight:"+headerHeight);
            var scrollToPosition = $scrollToElement.offset().top - headerHeight;
            $("html, body").animate({
              scrollTop: scrollToPosition
            }, 700, "swing", function(){setTimeout(function(){
                window.scrollBy(0, -2);
            }, 400) });
            /* To add/remove class */
            $('.menuItem').removeClass('selected'); // first remove class from all menu items
            $(this).children('.menuItem').addClass('selected'); // then add to the clicked item
            return false;
          }
        });
    });

    //popup
    //$(".linek-cont").css("z-index", "100");
    //$(".linek-cont").css("position", "fixed");
    //$(".linek-cont").css("width", "100%");
    //$(".linek-cont").css("height", "80%");

/*

    $(".linek-cont").hide();

//    var orig = $(".lineimg").prop("src");
//    var orig = $(".lineimge").prop("src");
    var orig = $(".linemapa").prop("src");
/*    $(".linek").click(function(ev){
      ev.preventDefault();
      var hash = "";
      if (this.hash){
        hash = this.hash.substr(1);
        console.log("hash linek: "+hash);
        $("#"+hash).show();
        console.log("called with: "+"#"+hash);
        $(".lineimg").prop("src");
      }
    });
    */
/*    $(".linek").click(function(ev){
      ev.preventDefault();
      var hash = "";
      if (this.hash){
        hash = this.hash.substr(1);
        console.log("hash linek: "+hash);
        $("#"+hash).show();
        console.log("called with: "+"#"+hash);
        $(".lineimge").prop("src");
      }
    });
    */
/*
    $(".linek").click(function(ev){
      ev.preventDefault();
      var hash = "";
      if (this.hash){
        hash = this.hash.substr(1);
        console.log("hash linek: "+hash);
        $("#"+hash).show();
        console.log("called with: "+"#"+hash);
        $(".linemapa").prop("src");
      }
    });*/

  /*  $(".modal-close").click(function(){
      $(this).parent().parent().hide();
      $(".lineimg").prop("");
    });*/
/*    $(".modal-close").click(function(){
      $(this).parent().parent().hide();
      $(".lineimge").prop("");
    });*/
  /*  $(".modal-close").click(function(){
      $(this).parent().parent().hide();
      $(".linemapa").prop("");
    });
*/
    //interactive modal
/*    $(".prod-img").hide();
    $(".prod-imge").hide();
    $(".mapa-img").hide();*/

  /*  $(".line-links a").click(function(ev){
      ev.preventDefault();
      $(".lineimg").prop("src", $(this).find(".prod-img").prop("src") );
    });*/

/*    $(".line-links a").click(function(ev){
      ev.preventDefault();
      if (this.hash){
        hash = this.hash.substr(1);
        console.log("hash linek: "+hash);
        $("#"+hash).show();
        console.log("called with: "+"#"+hash);
        $(".lineimge").prop("src", $(this).find(".prod-imge").prop("src") );
      }
    });*/

  /*  $(".mapa-links a").click(function(ev){
      ev.preventDefault();
      $(".linemapa").prop("src", $(this).find(".mapa-img").prop("src") );
    });
*/
    //headroom
    var header = document.querySelector("#header");
    new Headroom(header, {
      offset: 3,
      tolerance: {
        down : 1,
        up : 1
      },
      offset : 1,
      classes: {
        initial: "slide",
        pinned: "slide--reset",
        unpinned: "slide--up",
        top: "slide--top",
        notTop: "slide--notop"
      }
    }).init();
});
