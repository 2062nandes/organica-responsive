<footer >
    <div class="wrap wrapeverything">
        <div class="colf1">
            <div class="freewrapper" id="menupie">
                <div class="col25f">
                    <ul>
                        <li><a class="cat jlink" href="snacks.php#snacks">Snacks</a></li>
                        <li><a href="#linea-sabrositas">Línea Sabrositas</a></li>
                        <li><a href="#linea-fiesta">Línea Fiesta</a></li>
                        <li><a href="#linea-gourmet">Línea Gourmet</a></li>
                        <li><a href="#linea-recreo">Línea Recreo</a></li>
                        <li><a href="#linea-tossitos">Línea Tossitos</a></li>
                    </ul>
                </div>
                <div class="col25f">
                    <ul>
                        <li><a class="cat jlink" href="cereales.php#cereales">Cereales</a></li>
                        <li><a href="#linea-c-real">Línea Cereales</a></li>
                        <li><a href="#linea-sr-maiz">Línea Sr. Maíz</a></li>
                    </ul>
                </div>
                <div class="col25f">
                    <ul>
                        <li><a class="cat jlink" href="gramineas.php#gramineas">Gramíneas</a></li>
                        <li><a href="#linea-en-grano">En grano</a></li>
                        <li><a href="#linea-molidos">Molidos</a></li>
                        <li><a href="#linea-deshidratados">Deshidratados</a></li>
                        <li><a href="#linea-semillas">Semillas</a></li>
                    </ul>
                </div>
                <div class="col25f">
                    <ul>
                        <li><a class="cat jlink" href="corporativos.php">Corporativos</a></li>
                        <li><a href="#linea-personalizada">Línea Personalizada</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="colf2">
            <h2>DIRECCIÓN COCHABAMBA</h2>
            <p class="dire"> Zona Sumumpaya Sud<br>c. Libertador Bolívar  <a class="vmapa jlink" href="contacto.php#ubiquenos"><i class="fa fa-map-marker"></i> Ver mapa</a><br><i class="fa fa-phone"></i> Telf. (591-4) 4379000 <br><i class="fa fa-fax"></i> Fax: 4379001</p>
        </div>
        <div class="colf3">
            <h2>TODOS LOS DERECHOS RESERVADOS</h2>
            <p class="dire"> Copyright &COPY; 2016<br> Orgánica del Sur S.R.L. &REG;<br><span class="public"><a href="http://ahpublic.com" target="_blank">Diseńo y programación Ah! Publicidad</a></span></p>
        </div>
    </div>
    </footer>
    <div id="ir-arriba">
      <a class="jlink" href="#inicio"><span></span></a>
    </div>
</body>
</html>
