<?php require_once 'header.php'; ?>
<body id="inicio">
<?php require('menu.php') ?>
<div class="wrapeverything orswrapper">
	<div class="wrap maincontent">

		<?php //require('linea/snacks/linea-sabrositas.php'); ?>
		<?php //require('linea/snacks/linea-fiesta.php'); ?>
		<?php //require('linea/snacks/linea-gourmet.php'); ?>
		<?php //require('linea/snacks/linea-recreo.php'); ?>
		<?php //require('linea/snacks/linea-tossitos.php'); ?>
		<?php //require('linea/cereales/linea-cereales.php'); ?>
		<?php //require('linea/cereales/linea-sr-maiz.php'); ?>
		<?php //require('linea/gramineas/en-grano.php'); ?>
		<?php //require('linea/gramineas/molidos.php'); ?>
		<?php //require('linea/gramineas/deshidratados.php'); ?>
		<?php //require('linea/gramineas/semillas.php'); ?>
		<?php //require('linea/corporativos/linea-personalizada.php'); ?>
		<h2 id="contactenos">CONTÁCTENOS</h2>
		<?php require('linea.php'); ?>
    <div id="contactos" class="mainarticle nopad animateblock">
        <form method="post" id="theForm" class="second" action="mail.php" role="form">
        <div class="freewrapper">
            <div class="row">
                <div class="col50 colfor1">
                  <div class="form_row">
                    <div class="input">
                      <input type="text" id="nombre" name="nombre" tabindex="1" required>
                      <label for="nombre">Nombre completo:</label>
                    </div>
                  </div>

                  <div class="form_row">
                    <div class="input">
                      <input type="text" id="telefono" name="telefono" tabindex="2" required>
                      <label for="telefono">Teléfono:</label>
                    </div>
                  </div>

                  <div class="form_row">
                    <div class="input">
                      <input type="text" id="movil" name="movil" tabindex="3" required>
                      <label for="movil">Teléfono móvil:</label>
                    </div>
                  </div>

                  <div class="form_row">
                    <div class="input">
                      <input type="text" id="direccion" name="direccion" tabindex="4" required>
                      <label for="direccion">Dirección:</label>
                    </div>
                  </div>

                  <div class="form_row">
                    <div class="input">
                      <input type="text" id="ciudad" name="ciudad" tabindex="5" required>
                      <label for="ciudad">Ciudad:</label>
                    </div>
                  </div>
                </div><!-- fin de colf1 -->
                <div class="col50 colfor2">
                  <div class="form_row">
                    <div class="input">
                      <input type="email" id="email" name="email" tabindex="6" required>
                      <label for="email">Su e-mail:</label>
                    </div>
                  </div>

                  <div class="form_row mensaje">
                    <div class="input">
                      <textarea id="mensaje" cols="55" rows="7" name="mensaje" tabindex="7" required></textarea>
                      <label for="mensaje">Mensaje:</label>
                    </div>
                  </div>

                  <div class="form_row botones">
                    <input class="submitbtn" type="submit" tabindex="8" value="Enviar">
                    <input class="deletebtn" type="reset" tabindex="9" value="Borrar">
                  </div>
                </div><!-- fin de colf2 -->
            </div><!-- fin de row -->
        </div><!-- fin de formwrapper -->
        </form>
        <div id="statusMessage"></div>
    </div>
<h2 id="ubiquenos">ENCUENTRANOS EN TODO EL PAÍS</h2>
  <!--Galeria de mapas-->
  <div class="coll3">
	    <h3>Sucursales en todas las ciudades</h3>
		    <div class="mapa-bolivia">
					<img style="position:absolute;"src="img/mapa-bolivia.png" />
					<div class="mapa-links animated infinite pulse">
						<a id="la-paz" class="p-la-paz hint--top-right hint--medium" aria-label="Dirección: c. J.C. Valdez Nro.49475 - Teléfono: 70106197-70130685" href="#la-paz">
             <span><p><br>La Paz</p></span>
		        </a>
		         <a id="santa-cruz" class="p-santa-cruz hint--top-right hint--medium" aria-label="Dirección: B.Chaco sur calle 6 Nro. 4 Deposito zona parque industrial y virgen de Cotoca. - Teléfono:'70613981'" href="#santa-cruz">
		             <span><p><br>Santa Cruz <br>de la Sierra</p></span>
		          </a>
		          <a id="riberalta" class="p-pando hint--right hint--medium" aria-label="Dirección: Av.Trinidad a media cuadra de la plazuela Conavi. - Teléfono: 74404020" href="#pando">
		            <span><p><br>Riberalta</p></span>
		          </a>
		          <a id="tarija" class="p-tarija hint--right hint--medium" aria-label="Dirección: c.Luis Campero Nro.323  barrio defensores del chaco. - Teléfono:'72980330'" href="#tarija">
		            <span><p><br>Tarija</p></span>
		          </a>
		          <a id="trinidad" class="p-beni hint--top-right hint--medium" aria-label="Dirección: Av.Cambodromo Urb universitaria. - Teléfono: 69396205" href="#beni">
		            <span><p><br>Trinidad</p></span>
		          </a>
		          <a id="cochabamba" class="p-cochabamba hint--right hint--medium" aria-label="Dirección: Av. Blaco Galindo Km.8 1/2 y calle libertador Bolivar. - Teléfono:'67226000'" href="#cochabamba">
		            <span><p><br>Cochabamba</p></span>
		          </a>
		          <a id="oruro" class="p-oruro hint--bottom hint--medium" aria-label="Dirección: 6 de Octubre # 379, Villarroel y Oblitas - Teléfono: 67226000" href="#oruro">
		            <span><p><br>Oruro</p></span>
		          </a>
		          <a id="potosi" class="p-potosi hint--bottom-left hint--medium"
		          aria-label="Dirección: Av.Union Nro.432 zona facultad domingo Savio. - Teléfono: 70116915" href="#potosi">
		            <span><p><br>Potosi</p></span>
		          </a>
		          <a id="sucre" class="p-chuquisaca hint--top-right hint--medium" aria-label="Dirección: Sebastian Gracia # 588 zona estadium patria. - Teléfono: 70116915" href="#chuquisaca">
		            <span><p><br>Sucre</p></span>
		          </a>
          </div>
        </div>
  </div>
<!--Mapa en vista-->
    <div class="colmap">
      <div id="contenido-mapa"></div>
    </div>
</div>
</div>
<div class="clear"></div>
<?php require_once 'footer.php'; ?>
