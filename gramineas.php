<?php require_once 'header.php'; ?>
<body id="inicio">
<?php require('menu.php') ?>
<div class="wrapeverything orswrapper">
	<div class="wrap maincontent">
		<h2 id="gramineas" class="tituloProd">Gramíneas</h2>
		<div class="col100">
			<h3 id="en-grano">EN GRANO  <a href="#linea-en-grano"> <i class="ojo fa fa-eye" aria-hidden="true"></i></a></h3>
			<ul class="lb-album">
				<li class="col20d">
					<a href="#en-grano-1">
						<img src="img/productos/arroz.png" width="135" height="135" alt="en-grano-1">
						<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
					</a>
					<p>Arroz<p>
					<div class="lb-overlay " id="en-grano-1">
					<div class="line-inner">
						<div class="freewrapper1">
									<div class="coll1">
										<h2>Arroz</h2>
										<img class="lineimg" src="img/productos/arroz.png" alt="en-grano-1">
										<a href="#en-grano-8" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#en-grano-2" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
									</div>
									<div class="coll2 linea-scroll">
										<h3>EN GRANO </h3>
										<div class="line-links">
												<img class="lineimg" src="img/productos/arroz.png" alt="producto">
											</div>
												<div class="col-descripcion"><strong>Presentacion</strong>
												<p> 1000g, 440g</p>
												<strong>Descripcion</strong>
												<p>
													El arroz es la semilla de la planta Oryza sativa, al cual se le ha removido totalmente la cáscara. El cual es fraccionado y envasado.
												</p>
									</div>
								</div>
								<a href="#en-grano" class="lb-close modal-close"></i></a>
							</div>
					</div>
					</div>
				</li>
				<li class="col20d">
					<a href="#en-grano-2">
						<img src="img/productos/azucar-impalpable.png" width="135" height="135" alt="en-grano-2">
						<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
					</a>
					<p>Azúcar impalpable<p>
					<div class="lb-overlay " id="en-grano-2">
					<div class="line-inner">
						<div class="freewrapper1">
									<div class="coll1">
										<h2>Azúcar impalpable</h2>
										<img class="lineimg" src="img/productos/azucar-impalpable.png" alt="en-grano-2">
										<a href="#en-grano-1" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#en-grano-3" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
									</div>
									<div class="coll2 linea-scroll">
										<h3>EN GRANO </h3>
										<div class="line-links">
												<img class="lineimg" src="img/productos/azucar-impalpable.png" alt="producto">
											</div>
												<div class="col-descripcion"><strong>Presentacion</strong>
												<p> 200g</p>
												<strong>Descripcion</strong>
												<p>
														Azúcar impalpable es obtenido del azúcar refinado de caña, a través de un proceso de molienda extrafina. El cual es fraccionado y envasado.
												</p>
									</div>
								</div>
								<a href="#en-grano" class="lb-close modal-close"></i></a>
							</div>
					</div>
					</div>
				</li>
				<li class="col20d">
					<a href="#en-grano-3">
						<img src="img/productos/garbanzos.png" width="135" height="135" alt="en-grano-3">
						<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
					</a>
					<p>Garbanzos<p>
					<div class="lb-overlay " id="en-grano-3">
					<div class="line-inner">
						<div class="freewrapper1">
									<div class="coll1">
										<h2>Garbanzos</h2>
										<img class="lineimg" src="img/productos/garbanzos.png" alt="en-grano-3">
										<a href="#en-grano-2" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#en-grano-4" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
									</div>
									<div class="coll2 linea-scroll">
										<h3>EN GRANO </h3>
										<div class="line-links">
												<img class="lineimg" src="img/productos/garbanzos.png" alt="producto">
											</div>
												<div class="col-descripcion"><strong>Presentacion</strong>
												<p> 150g</p>
												<strong>Descripcion</strong>
												<p>
														El garbanzo es el grano seco y maduro procedente de las especies Cicer arietinum. El cual es fraccionado y envasado.
												</p>
									</div>
								</div>
								<a href="#en-grano" class="lb-close modal-close"></i></a>
							</div>
					</div>
					</div>
				</li>
				<li class="col20d">
					<a href="#en-grano-4">
						<img src="img/productos/lentejas.png" width="135" height="135" alt="en-grano-4">
						<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
					</a>
					<p>Lentejas<p>
					<div class="lb-overlay " id="en-grano-4">
					<div class="line-inner">
						<div class="freewrapper1">
									<div class="coll1">
										<h2>Lentejas</h2>
										<img class="lineimg" src="img/productos/lentejas.png" alt="en-grano-4">
										<a href="#en-grano-3" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#en-grano-5" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
									</div>
									<div class="coll2 linea-scroll">
										<h3>EN GRANO </h3>
										<div class="line-links">
												<img class="lineimg" src="img/productos/lentejas.png" alt="producto">
											</div>
												<div class="col-descripcion"><strong>Presentacion</strong>
												<p> 400g</p>
												<strong>Descripcion</strong>
												<p>
														La lenteja es el grano seco y maduro procedente de la especie Lens culinaria. El cual es fraccionado y envasado.
												</p>
									</div>
								</div>
								<a href="#en-grano" class="lb-close modal-close"></i></a>
							</div>
				  	</div>
					</div>
				</li>
				<li class="col20d">
					<a href="#en-grano-5">
						<img src="img/productos/trigos-enteros.png" width="135" height="135" alt="en-grano-5">
						<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
					</a>
					<p>Trigos pelado entero<p>
					<div class="lb-overlay " id="en-grano-5">
					<div class="line-inner">
						<div class="freewrapper1">
									<div class="coll1">
										<h2>Trigo pelado entero</h2>
										<img class="lineimg" src="img/productos/trigos-enteros.png" alt="en-grano-5">
										<a href="#en-grano-4" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#en-grano-6" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
									</div>
									<div class="coll2 linea-scroll">
										<h3>EN GRANO </h3>
										<div class="line-links">
												<img class="lineimg" src="img/productos/trigos-enteros.png" alt="producto">
											</div>
												<div class="col-descripcion"><strong>Presentacion</strong>
												<p> 350g</p>
												<strong>Descripcion</strong>
												<p>
														Trigo pelado entero es obtenido de los granos del genero Triticum, que pasa por un proceso de lavado, friccionando o remojando en solución alcalina caliente, lavando con agua y secado. El cual es fraccionado y envasado.
												</p>
									</div>
								</div>
								<a href="#en-grano" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
				</li>
				<li class="col20d">
					<a href="#en-grano-6">
						<img src="img/productos/trigos-partidos.png" width="135" height="135" alt="en-grano-6">
						<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
					</a>
					<p>Trigo pelado partido<p>
					<div class="lb-overlay " id="en-grano-6">
					<div class="line-inner">
						<div class="freewrapper1">
									<div class="coll1">
										<h2>Trigo pelado partido</h2>
										<img class="lineimg" src="img/productos/trigos-partidos.png" alt="en-grano-6">
										<a href="#en-grano-5" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#en-grano-7" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
									</div>
									<div class="coll2 linea-scroll">
										<h3>EN GRANO </h3>
										<div class="line-links">
												<img class="lineimg" src="img/productos/trigos-partidos.png" alt="producto">
											</div>
												<div class="col-descripcion"><strong>Presentacion</strong>
												<p> 400g</p>
												<strong>Descripcion</strong>
												<p>
														Trigo pelado partido es obtenido de los granos del genero Triticum, que pasa por un proceso de lavado, friccionando o remojando en solución alcalina caliente, lavando con agua, secado y triturado. El cual es fraccionado y envasado.
												</p>
									</div>
								</div>
								<a href="#en-grano" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
				</li>
				<li class="col20d">
					<a href="#en-grano-7">
						<img src="img/productos/foto.jpg" width="135" height="135" alt="en-grano-1">
						<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
					</a>
					<p>Maíz para somo<p>
					<div class="lb-overlay " id="en-grano-7">
					<div class="line-inner">
						<div class="freewrapper1">
									<div class="coll1">
										<h2>Maíz para somo</h2>
										<img class="lineimg" src="img/productos/foto.jpg" alt="en-grano-7">
										<a href="#en-grano-6" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#en-grano-8" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
									</div>
									<div class="coll2 linea-scroll">
										<h3>EN GRANO </h3>
										<div class="line-links">
												<img class="lineimg" src="img/productos/foto.jpg" alt="producto">
											</div>
												<div class="col-descripcion"><strong>Presentacion</strong>
												<p> 200g</p>
												<strong>Descripcion</strong>
												<p>
														Producto obtenido a partir de los granos de maíz blanco totalmente pelado y desgerminado. El cual es fraccionado y envasado
												</p>
									</div>
								</div>
								<a href="#en-grano" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
				</li>
				<li class="col20d">
					<a href="#en-grano-8">
						<img src="img/productos/maiz-pipocas.png" width="135" height="135" alt="en-grano-8">
						<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
					</a>
					<p>Maíz pipoca<p>
					<div class="lb-overlay " id="en-grano-8">
					<div class="line-inner">
						<div class="freewrapper1">
									<div class="coll1">
										<h2>Maíz pipoca</h2>
										<img class="lineimg" src="img/productos/maiz-pipocas.png" alt="en-grano-8">
										<a href="#en-grano-7" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
										<a href="#en-grano-1" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
									</div>
									<div class="coll2 linea-scroll">
										<h3>EN GRANO </h3>
										<div class="line-links">
												<img class="lineimg" src="img/productos/maiz-pipocas.png" alt="producto">
											</div>
												<div class="col-descripcion"><strong>Presentacion</strong>
												<p> 350g 150g</p>
												<strong>Descripcion</strong>
												<p>
														El maíz pipoca, reventón o pisingallo es el grano maduro procedente de la especie de maíz Zea mays everata. El cual es fraccionado y envasado.
												</p>
									</div>
								</div>
								<a href="#en-grano" class="lb-close modal-close"></i></a>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
<!----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
<div class="col100">
	<h3 id="molidos">MOLIDOS <a href="#linea-molidos"> <i class="ojo fa fa-eye" aria-hidden="true"></i></a></h3>
	<ul class="lb-album">
		<li class="col20d">
			<a href="#molidos-1">
				<img src="img/productos/harina-blanca.png" width="135" height="135" alt="molidos01">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Harina blanca<p>
			<div class="lb-overlay " id="molidos-1">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Harina blanca</h2>
								<img class="lineimg" src="img/productos/harina-blanca.png" alt="molidos01">
								<a href="#molidos-10" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#molidos-2" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>MOLIDOS</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/harina-blanca.png" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> </p>
										<strong>Descripcion</strong>
										<p>

										</p>
							</div>
						</div>
						<a href="#molidos" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#molidos-2">
				<img src="img/productos/lawa-de-choclo.png" width="135" height="135" alt="molidos02">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Harina para lawa de choclo<p>
			<div class="lb-overlay " id="molidos-2">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Harina para lawa de choclo</h2>
								<img class="lineimg" src="img/productos/lawa-de-choclo.png" alt="molidos02">
								<a href="#molidos-1" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#molidos-3" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>MOLIDOS</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/lawa-de-choclo.png" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> 250g</p>
										<strong>Descripcion</strong>
										<p>
												Producto obtenido de granos de maíz blanco (Zea mays), enteros y maduros, que pasan por un proceso de molturación en el que se tritura finamente. Al cual se añaden especias, se fracciona y envasa.
										</p>
							</div>
						</div>
						<a href="#molidos" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#molidos-3">
				<img src="img/productos/lawas-de-chunio.png" width="135" height="135" alt="molidos03">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Harina para lawa de chuño<p>
			<div class="lb-overlay " id="molidos-3">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Harina para lawa de chuño</h2>
								<img class="lineimg" src="img/productos/lawas-de-chunio.png" alt="molidos03">
								<a href="#molidos-2" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#molidos-4" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>MOLIDOS</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/lawas-de-chunio.png" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> 250g</p>
										<strong>Descripcion</strong>
										<p>
												La harina de chuño es obtenido a partir del chuño negro, que pasa por una molienda fina. El cual es fraccionado y envasado.
										</p>
							</div>
						</div>
						<a href="#molidos" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#molidos-4">
				<img src="img/productos/linazas-molidas.png" width="135" height="135" alt="molidos04">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Linaza molida<p>
			<div class="lb-overlay " id="molidos-4">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Linaza molida</h2>
								<img class="lineimg" src="img/productos/linazas-molidas.png" alt="molidos04">
								<a href="#molidos-3" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#molidos-5" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>MOLIDOS</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/linazas-molidas.png" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> 500g, 230g, 100g</p>
										<strong>Descripcion</strong>
										<p>
												La linaza es obtenida a partir de las semillas de la planta de lino (Linum usitatissimum), este pasa por un proceso de retostado y molido. El cual es fraccionado y envasado.
										</p>
							</div>
						</div>
						<a href="#molidos" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#molidos-5">
				<img src="img/productos/pan-duro.png" width="135" height="135" alt="molidos05">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Pan duro<p>
			<div class="lb-overlay " id="molidos-5">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Pan duro</h2>
								<img class="lineimg" src="img/productos/pan-duro.png" alt="molidos05">
								<a href="#molidos-4" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#molidos-6" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>MOLIDOS</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/pan-duro.png" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> 200g</p>
										<strong>Descripcion</strong>
										<p>
												Producto obtenido a partir de la molienda fina de pan seco, el cual fraccionado y envasado.
										</p>
							</div>
						</div>
						<a href="#molidos" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#molidos-6">
				<img src="img/productos/foto.jpg" width="135" height="135" alt="molidos06">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Sémola blanca<p>
			<div class="lb-overlay " id="molidos-6">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Sémola blanca</h2>
								<img class="lineimg" src="img/productos/foto.jpg" alt="molidos06">
								<a href="#molidos-5" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#molidos-7" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>MOLIDOS</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/foto.jpg" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> 300g</p>
										<strong>Descripcion</strong>
										<p>
												Producto obtenido a partir de la molienda fina del maíz blanco. El cual es fraccionado y envasado.
										</p>
							</div>
						</div>
						<a href="#molidos" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#molidos-7">
				<img src="img/productos/foto.jpg" width="135" height="135" alt="molidos07">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Tojoris<p>
			<div class="lb-overlay " id="molidos-7">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Tojoris</h2>
								<img class="lineimg" src="img/productos/foto.jpg" alt="molidos07">
								<a href="#molidos-6" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#molidos-8" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>MOLIDOS</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/foto.jpg" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> 300g</p>
										<strong>Descripcion</strong>
										<p>
												Producto obtenido a partir de los granos de maíz (Zea mays) variedad willcaparu, enteros y maduros, que pasan por un proceso de trituración. El cual se fracciona y envasa.
										</p>
							</div>
						</div>
						<a href="#molidos" class="lb-close modal-close"></i></a>
					</div>
					</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#molidos-8">
				<img src="img/productos/foto.jpg" width="135" height="135" alt="molidos08">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Avena en hojuelas<p>
			<div class="lb-overlay " id="molidos-8">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Avena en hojuelas</h2>
								<img class="lineimg" src="img/productos/foto.jpg" alt="molidos08">
								<a href="#molidos-7" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#molidos-9" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>MOLIDOS</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/foto.jpg" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> </p>
										<strong>Descripcion</strong>
										<p>
												Producto obtenido de los granos de avena (Avena sativa o Avena bizantina), que pasan por un proceso de limpiado, secado, descascarados, cortados transversalmente y que han sido aplastados para formar las hojuelas, escamas o copos. El cual es fraccionado y envasado.
										</p>
							</div>
						</div>
						<a href="#molidos" class="lb-close modal-close"></i></a>
					</div>
					</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#molidos-9">
				<img src="img/productos/jankaquipa.png" width="135" height="135" alt="molidos09">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Harina para lawa de jankaquipa<p>
			<div class="lb-overlay " id="molidos-9">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Harina para lawa de jankaquipa</h2>
								<img class="lineimg" src="img/productos/jankaquipa.png" alt="molidos09">
								<a href="#molidos-8" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#molidos-10" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>MOLIDOS</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/jankaquipa.png" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> 350g</p>
										<strong>Descripcion</strong>
										<p>
												Producto obtenido a partir de los granos de maíz (Zea mays) variedad willcaparu, enteros y maduros, que pasan por un proceso de molturación en el que se tritura finamente. El cual se fracciona y envasa.
										</p>
							</div>
						</div>
						<a href="#molidos" class="lb-close modal-close"></i></a>
					</div>
					</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#molidos-10">
				<img src="img/productos/coco-rallado.png" width="135" height="135" alt="molidos010">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Coco rallado<p>
			<div class="lb-overlay " id="molidos-10">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Coco rallado</h2>
								<img class="lineimg" src="img/productos/coco-rallado.png" alt="molidos010">
								<a href="#molidos-9" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#molidos-1" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>MOLIDOS</h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/coco-rallado.png" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> 60g</p>
										<strong>Descripcion</strong>
										<p>
												El coco rallado es obtenido de los frutos de la planta de coco (Cocos nucífera), que pasa por un proceso industrial de pelado, rallado fino y secado. El cual es fraccionado y envasado.
										</p>
							</div>
						</div>
						<a href="#molidos" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
	</ul>
</div>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
		<div class="col100">
				<h3 id="deshidratados">DESHIDRATADOS<a href="#linea-deshidratados"> <i class="ojo fa fa-eye" aria-hidden="true"></i></a></h3>
				<ul class="lb-album">
					<li class="col20d">
						<a href="#deshidratado-1">
							<img src="img/productos/chunios-negros.png" width="135" height="135" alt="deshidratado01">
							<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
						</a>
						<p>Chuños negros</p>
						<div class="lb-overlay " id="deshidratado-1">
						<div class="line-inner">
							<div class="freewrapper1">
										<div class="coll1">
											<h2>Chuños negros</h2>
											<img class="lineimg" src="img/productos/chunios-negros.png" alt="deshidratado01">
											<a href="#deshidratado-3" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
											<a href="#deshidratado-2" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
										</div>
										<div class="coll2 linea-scroll">
											<h3>DESHIDRATADOS</h3>
											<div class="line-links">
													<img class="lineimg" src="img/productos/chunios-negros.png" alt="producto">
												</div>
													<div class="col-descripcion"><strong>Presentacion</strong>
													<p> 130g</p>
													<strong>Descripcion</strong>
													<p>
															El chuño negro es obtenido de una amplia variedades de tubérculos de papa, que pasan por un proceso artesanal de congelación, deshidratación y secado con exposición al sol. El cual es fraccionado y envasado.
													</p>
										</div>
									</div>
									<a href="#deshidratados" class="lb-close modal-close"></i></a>
								</div>
							</div>
						</div>
					</li>
					<li class="col20d">
						<a href="#deshidratado-2">
							<img src="img/productos/mocochinchi.png" width="135" height="135" alt="deshidratado02">
							<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
						</a>
						<p>Mocochinchi</p>
						<div class="lb-overlay " id="deshidratado-2">
						<div class="line-inner">
							<div class="freewrapper1">
										<div class="coll1">
											<h2>Mocochinchi</h2>
											<img class="lineimg" src="img/productos/mocochinchi.png" alt="deshidratado02">
											<a href="#deshidratado-1" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
											<a href="#deshidratado-3" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
										</div>
										<div class="coll2 linea-scroll">
											<h3>DESHIDRATADOS</h3>
											<div class="line-links">
													<img class="lineimg" src="img/productos/mocochinchi.png" alt="producto">
												</div>
													<div class="col-descripcion"><strong>Presentacion</strong>
													<p> 180g</p>
													<strong>Descripcion</strong>
													<p>
															El mocochinchi es obtenido del fruto del durazno (Prunus persica), que pasa por un proceso de semi- pelado y deshidratado al sol. El cual es fraccionado y envasado.
													</p>
										</div>
									</div>
									<a href="#deshidratados" class="lb-close modal-close"></i></a>
								</div>
							</div>
						</div>
					</li>
					<li class="col20d">
						<a href="#deshidratado-3">
							<img src="img/productos/tuntas.png" width="135" height="135" alt="deshidratado03">
							<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
						</a>
						<p>Tunta</p>
						<div class="lb-overlay " id="deshidratado-3">
						<div class="line-inner">
							<div class="freewrapper1">
										<div class="coll1">
											<h2>Tunta</h2>
											<img class="lineimg" src="img/productos/tuntas.png" alt="deshidratado03">
											<a href="#deshidratado-2" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
											<a href="#deshidratado-1" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
										</div>
										<div class="coll2 linea-scroll">
											<h3>DESHIDRATADOS</h3>
											<div class="line-links">
													<img class="lineimg" src="img/productos/tuntas.png" alt="producto">
												</div>
													<div class="col-descripcion"><strong>Presentacion</strong>
													<p> 130g</p>
													<strong>Descripcion</strong>
													<p>
															El chuño tunta es obtenido de una amplia variedades de tubérculos de papa, que pasan por un proceso artesanal de deshidratación a través de sucesivos congelamientos (con protección solar), sumergido en agua corriente (río) y secado al sol. El cual es fraccionado y envasado.
													</p>
										</div>
									</div>
									<a href="#deshidratados" class="lb-close modal-close"></i></a>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
<!--------------------------------------------------------------------------------------------------------------------------------->
<div class="col100">
	<h3 id="semillas">SEMILLAS  <a href="#linea-semillas"> <i class="ojo fa fa-eye" aria-hidden="true"></i></a></h3>
	<ul class="lb-album">
		<li class="col20d">
			<a href="#semilla-1">
				<img src="img/productos/mani-con-cascara.png" width="135" height="135" alt="semilla01">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Manís con cascara<p>
			<div class="lb-overlay " id="semilla-1">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Manís con cascara</h2>
								<img class="lineimg" src="img/productos/mani-con-cascara.png" alt="semilla01">
								<a href="#semilla-5" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#semilla-2" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>SEMILLAS </h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/mani-con-cascara.png" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> </p>
										<strong>Descripcion</strong>
										<p>

										</p>
							</div>
						</div>
						<a href="#semillas" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#semilla-2">
				<img src="img/productos/mani-pelado.png" width="135" height="135" alt="semilla02">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Maní pelado<p>
			<div class="lb-overlay " id="semilla-2">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Maní pelado</h2>
								<img class="lineimg" src="img/productos/mani-pelado.png" alt="semilla02">
								<a href="#semilla-1" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#semilla-3" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>SEMILLAS </h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/mani-pelado.png" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> 300g 100g</p>
										<strong>Descripcion</strong>
										<p>
												El maní es grano maduro procedente de la especie Arachis hypogaea, que pasa por un proceso de pelado. El cual es fraccionado y envasado.
										</p>
							</div>
						</div>
						<a href="#semillas" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#semilla-3">
				<img src="img/productos/porotos-blancos.png" width="135" height="135" alt="semilla03">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Porotos blancos<p>
			<div class="lb-overlay " id="semilla-3">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Porotos blancos</h2>
								<img class="lineimg" src="img/productos/porotos-blancos.png" alt="semilla03">
								<a href="#semilla-2" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#semilla-4" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>SEMILLAS </h3>
									<div class="line-links">
										<img class="lineimg" src="img/productos/porotos-blancos.png" alt="producto">
									</div>
									<div class="col-descripcion"><strong>Presentacion</strong>
										<p> 350g</p>
										<strong>Descripcion</strong>
										<p>
												El frijol o poroto blanco es el grano seco y maduro de una de las variedades procedente de la planta de frejol (Phaseolus vulgaris). El cual es fraccionado y envasado.
										</p>
									</div>
						</div>
						<a href="#semillas" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
		<li class="col20d">
			<a href="#semilla-4">
				<img src="img/productos/porotos-negros.png" width="135" height="135" alt="semilla04">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Porotos negros<p>
			<div class="lb-overlay " id="semilla-4">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Porotos negros</h2>
								<img class="lineimg" src="img/productos/porotos-negros.png" alt="semilla04">
								<a href="#semilla-3" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#semilla-5" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>SEMILLAS </h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/porotos-negros.png" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> 350g</p>
										<strong>Descripcion</strong>
										<p>
												El frijol o poroto negro es el grano seco y maduro de una delas variedades procedente de la planta de frejol (Phaseolus vulgaris).El cual es fraccionado y envasado.
										</p>
							</div>
						</div>
						<a href="#semillas" class="lb-close modal-close"></i></a>
					</div>
			  </div>
			</div>
		</li>
		<li class="col20d">
			<a href="#semilla-5">
				<img src="img/productos/porotos-rojos.png" width="135" height="135" alt="semilla05">
				<span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
			</a>
			<p>Porotos rojos<p>
			<div class="lb-overlay " id="semilla-5">
			<div class="line-inner">
				<div class="freewrapper1">
							<div class="coll1">
								<h2>Porotos rojos</h2>
								<img class="lineimg" src="img/productos/porotos-rojos.png" alt="semilla05">
								<a href="#semilla-4" class="lb-prev"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#semilla-1" class="lb-next"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
							<div class="coll2 linea-scroll">
								<h3>SEMILLAS </h3>
								<div class="line-links">
										<img class="lineimg" src="img/productos/porotos-rojos.png" alt="producto">
									</div>
										<div class="col-descripcion"><strong>Presentacion</strong>
										<p> 350g</p>
										<strong>Descripcion</strong>
										<p>
												El frijol o poroto rojo es el grano seco y maduro de una de las variedades procedente de la planta de frejol (Phaseolus vulgaris). El cual es fraccionado y envasado.
										</p>
									</div>
								</div>
						<a href="#semillas" class="lb-close modal-close"></i></a>
					</div>
				</div>
			</div>
		</li>
	</div>
	</ul>
	<?php require('linea.php'); ?>
<!------------------------------------------------------------------------------------------------->
</div>
</div>
<div class="clear"></div>
<?php require_once 'footer.php'; ?>
