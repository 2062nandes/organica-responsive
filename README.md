***Web Site for ![O](https://gitlab.com/2062nandes/organica-responsive/raw/8bfea09f9371cc5db6a82dc73a2e8a29804ea500/favicon.png)rgánica del Sur***
-----------------------------------
**Theme Name:** Web Design & programing for Organica del Sur
**Theme URI:** [www.organicadelsur.com](http://www.organicadelsur.com/)

**Author:** Fernando Javier Averanga Aruquipa
**Author URI:** nandes.ingsistemas@gmail.com
**Gitlab:** [https://gitlab.com/2062nandes](https://gitlab.com/2062nandes)

![enter image description here](https://gitlab.com/2062nandes/organica-responsive/raw/8bfea09f9371cc5db6a82dc73a2e8a29804ea500/docs/screamshot.jpg)