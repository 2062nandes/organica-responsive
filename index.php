<?php require_once 'header.php'; ?>
<body id="inicio">
<div id="fakebg"><div class="circle"><div class="inner"></div> <div class="load-container load3"> <div class="loader">Cargando...</div></div></div></div>
<?php require('menu.php') ?>
<div class="wrapeverything orswrapper">
  <div class="wrap maincontent">
    <div id="amazingslider-wrapper-1" style="display:block;position:relative;width:100%;margin:0 auto;margin-top: 32px;box-shadow: none;    background: none;    margin-top: 0;    border-top-left-radius: 0;    border-top-right-radius: 0;    border: none;padding: 0;">
        <div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
            <ul class="amazingslider-slides" style="display:none;">
					<!-- <li><img src="images/slide.jpg" alt="Bienvenido al sitio web de ORGÁNICA DEL SUR" /></li>-->
								<li><img src="images/slide0.jpg" alt="Orgánica del Sur S.R.L." /></li>
                <li><img src="images/slide1.jpg" alt="Snacks, Chesitos Fiesta" /></li>
                <li><img src="images/slide2.jpg" alt="Papas fritas en varios sabores" /></li>
          <!--  <li><img src="images/slide3.jpg" alt="Papas fritas en varios sabores" /></li>
                <li><img src="images/slide4.jpg" alt="Maní con pasas, salado y picante" /></li>
                <li><img src="images/slide5.jpg" alt="Nachos de maíz en varios sabores" /></li>
                <li><img src="images/slide6.jpg" alt="Papas fritas sabrositas, de Wilsterman y del Bolívar" /></li>
                <li><img src="images/slide7.jpg" alt="Pipocas con miel, sabrochetos y pasank&apos;alla tossitos" /></li>
								<li><img src="images/slide8.jpg" alt="Orgánica del Sur S.R.L." /></li>
						-->	<li><img src="images/slide9.jpg" alt="Arroz, Azucar, Mani - Línea Don vargas" /></li>
					<!--	<li><img src="images/slide10.jpg" alt="C-Real - Bolitas de chocolate, aritos de frutas" /></li>
							<li><img src="images/slide11.jpg" alt="Orgánica del Sur S.R.L." /></li>-->
            </ul>
            <ul class="amazingslider-thumbnails" style="display:none;">
					<!--  <li><img src="images/slide-tn.jpg" alt="Bienvenido al sitio web de ORGÁNICA DEL SUR" /></li> -->
								<li><img src="images/slide0-tn.jpg" alt="Orgánica del Sur S.R.L." /></li>
                <li><img src="images/slide1-tn.jpg" alt="Snacks, Chesitos Fiesta" /></li>
                <li><img src="images/slide2-tn.jpg" alt="Papas fritas en varios sabores" /></li>
          <!--  <li><img src="images/slide3-tn.jpg" alt="Papas fritas en varios sabores" /></li>
                <li><img src="images/slide4-tn.jpg" alt="Maní con pasas, salado y picante" /></li>
                <li><img src="images/slide5-tn.jpg" alt="Nachos de maíz en varios sabores" /></li>
                <li><a class="linek" href="#papas-bolivar"><span>Papas Bolívar</span></a><img src="images/slide6-tn.jpg" alt="Papas fritas sabrositas, de Wilsterman y del Bolívar" /></li>
                <li><img src="images/slide7-tn.jpg" alt="Pipocas con miel, sabrochetos y pasank&apos;alla tossitos" /></li>
								<li><img src="images/slide8-tn.jpg" alt="Orgánica del Sur S.R.L." /></li>
					-->		<li><img src="images/slide9-tn.jpg" alt="Arroz, Azucar, Mani Pelado - Línea Don vargas" /></li>
					<!--  <li><img src="images/slide10-tn.jpg" alt="C-Real - Bolitas de chocolate, aritos de frutas" /></li>
					  	<li><img src="images/slide11-tn.jpg" alt="Orgánica del Sur S.R.L." /></li>-->
            </ul>
        </div>
    </div>
    <?php require('linea.php'); ?>
	</div>
</div>

<?php require('footer.php') ?>
